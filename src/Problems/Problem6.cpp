//ProjectEuler/ProjectEulerCPP/src/Problems/Problem6.cpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <cmath>
#include <sstream>
#include <string>
#include "Problems/Problem6.hpp"


int Problem6::START_NUM = 1;	//The first number to check
int Problem6::END_NUM = 100;	//The last number to check

//Constructor
Problem6::Problem6() : Problem("Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum."), sumOfSquares(0), squareOfSum(0){
}

//Solve the problem
void Problem6::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Run through all numbers and add them to the appropriate sums
	for(int currentNum = START_NUM;currentNum <= END_NUM;++currentNum){
		sumOfSquares += (currentNum * currentNum);	//Add the square to the correct variable
		squareOfSum += currentNum;	//Add the number to the correct variable for squaring later
	}
	//Square the sum that needs it
	squareOfSum *= squareOfSum;


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem6::reset(){
	Problem::reset();
	sumOfSquares = squareOfSum = 0;
}

//Gets
//Return a string with the solution to the problem
std::string Problem6::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The difference between the sum of the squares and the square of the sum of all numbers from 1-100 is " << abs(sumOfSquares - squareOfSum);
	return result.str();
}
//Returns the sum of all the squares
uint64_t Problem6::getSumOfSquares() const{
	solvedCheck("sum of the squares");
	return sumOfSquares;
}
//Returns the square of all of the sums
uint64_t Problem6::getSquareOfSum() const{
	solvedCheck("square of the sums");
	return squareOfSum;
}
//Returns the requested difference
uint64_t Problem6::getDifference() const{
	solvedCheck("difference between the two numbers");
	return std::abs((int64_t)(sumOfSquares - squareOfSum));
}
