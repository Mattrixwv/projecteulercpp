//ProjectEuler/ProjectEulerCPP/src/Problems/Problem3.cpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//The largest prime factor of 600851475143
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <sstream>
#include <string>
#include <vector>
#include "mee/numberAlgorithms.hpp"
#include "Problems/Problem3.hpp"


//The number of which you are trying to find the factors
uint64_t Problem3::GOAL_NUMBER = 600851475143;

//Constructor
Problem3::Problem3() : Problem("What is the largest prime factor of 600851475143?"){
}

//Solve the problem
void Problem3::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Find the factors of GOAL_NUMBER
	factors = mee::getFactors(GOAL_NUMBER);


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem3::reset(){
	Problem::reset();
	factors.clear();
}

//Gets
//Return a string with the solution to the problem
std::string Problem3::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The largest factor of the number " << GOAL_NUMBER << " is " << factors[factors.size() - 1];
	return result.str();
}
//Returns the list of factors of the number
std::vector<uint64_t> Problem3::getFactors() const{
	solvedCheck("factors");
	return factors;
}
//Returns the largest factor of the number
uint64_t Problem3::getLargestFactor() const{
	solvedCheck("largest factor");
	return *factors.end();
}
