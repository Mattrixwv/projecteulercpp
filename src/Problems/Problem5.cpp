//ProjectEuler/ProjectEulerCPP/src/Problems/Problem5.cpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <sstream>
#include <string>
#include <vector>
#include "mee/numberAlgorithms.hpp"
#include "mee/vectorAlgorithms.hpp"
#include "Problems/Problem5.hpp"


//Constructor
Problem5::Problem5() : Problem("What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?"), smallestNum(0){
}

//Solve the problem
void Problem5::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	int currentNum = 0;	//The current number being checked
	//Get all primes < 20 and multiply them together. That will be the starting point as the smallest possible answer
	std::vector<int> primes = mee::getPrimes(20);
	currentNum = mee::getProduct(primes);
	bool numFound = false;	//Used to determine if the number has been found
	while((currentNum > 0) && (!numFound)){
		//Start by assuming you found the number (because we throw a flag if we didn't find it)
		numFound = true;
		//Step through every number from 1-20 seeing if the current number is divisible by it
		for(int divisor = 1;divisor <= 20;++divisor){
			//If it is not divisible then throw a flag and start looking at the next number
			if((currentNum % divisor) != 0){
				numFound = false;
				break;
			}
		}
		//If you didn't find the correct number then increment by 2 because the number has to be even
		if(!numFound){
			currentNum += 2;
		}
	}
	//Save the current number as the smallest
	smallestNum = currentNum;


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem5::reset(){
	Problem::reset();
	smallestNum = 0;
}

//Gets
//Return a string with the solution to the problem
std::string Problem5::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The smallest positive number evenly divisible by all numbers 1-20 is " << smallestNum;
	return result.str();
}
//Returns the requested number
int Problem5::getNumber() const{
	solvedCheck("number");
	return smallestNum;
}
