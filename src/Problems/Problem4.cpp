//ProjectEuler/ProjectEulerCPP/src/Problems/Problem4.cpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//Find the largest palindrome made from the product of two 3-digit numbers
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <algorithm>
#include <sstream>
#include <string>
#include <vector>
#include "Problems/Problem4.hpp"


//The first number to check
int Problem4::START_NUM = 100;
//The last number to check
int Problem4::END_NUM = 999;

//Constructor
Problem4::Problem4() : Problem("Find the largest palindrome made from the product of two 3-digit numbers."){
}

//Solve the problem
void Problem4::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Start at the first 3-digit number and check every one up to the last 3-digit number
	for(int firstNum = START_NUM;firstNum <= END_NUM;++firstNum){
		//You can start at the location of the first number because everything before that has already been tested. (100*101 == 101*100)
		for(int secondNum = firstNum;secondNum < END_NUM;++secondNum){
			//Get the product
			uint64_t product = firstNum * secondNum;
			//Change the number to a string
			std::string productString= std::to_string(product);
			//Reverse the string
			std::string revProductString = productString;
			std::reverse(revProductString.begin(), revProductString.end());

			//If the number and it's reverse are the same it is a palindrome so add it to the vector
			if(productString == revProductString){
				palindromes.push_back(product);
			}
			//If it's not a palindrome ignore it and move to the next number
		}
	}
	//Sort the palindromes so that the last one is the largest
	std::sort(palindromes.begin(), palindromes.end());


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Resets the problem so it can be run again
void Problem4::reset(){
	Problem::reset();
	palindromes.clear();
}

//Gets
//Return a string with the solution to the problem
std::string Problem4::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The largest palindrome is " << *(palindromes.end() - 1);
	return result.str();
}
//Returns the list of all palindromes
std::vector<uint64_t> Problem4::getPalindromes() const{
	solvedCheck("palindromes");
	return palindromes;
}
//Returns the largest palindrome
uint64_t Problem4::getLargestPalindrome() const{
	solvedCheck("largest palindrome");
	return *(palindromes.end() - 1);
}
