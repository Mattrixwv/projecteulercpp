//ProjectEuler/ProjectEulerCPP/src/Problems/Problem14.cpp
//Matthew Ellison
// Created: 09-29-18
//Modified: 07-02-21
/*
The following iterative sequence is defined for the set of positive integers:
n → n/2 (n is even)
n → 3n + 1 (n is odd)
Which starting number, under one million, produces the longest chain?
*/
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <sstream>
#include <string>
#include "Problems/Problem14.hpp"


//This is the top number that you will be checking against the series
uint64_t Problem14::MAX_NUM = 1000000 - 1;

//This function follows the rules of the sequence and returns its length
uint64_t Problem14::checkSeries(uint64_t num){
	uint64_t length = 1;	//Start at 1 because you need to count the starting number

	//Follow the series, adding 1 for each step you take
	while(num > 1){
		if((num % 2) == 0){
			num /= 2;
		}
		else{
			num = (3 * num) + 1;
		}
		++length;
	}
	return length;
}

//Constructor
Problem14::Problem14() : Problem("Which starting number, under one million, produces the longest chain using the itterative sequence?"), maxLength(0), maxNum(0){
}

//Solve the problem
void Problem14::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Loop through all numbers less than 1000000 and check them agains the series
	for(uint64_t currentNum = 1;currentNum <= MAX_NUM;++currentNum){
		uint64_t currentLength = checkSeries(currentNum);
		//If the current number has a longer series than the max then the current becomes the max
		if(currentLength > maxLength){
			maxLength = currentLength;
			maxNum = currentNum;
		}
	}


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem14::reset(){
	Problem::reset();
	maxLength = 0;
	maxNum = 0;
}

//Gets
//Return a string with the solution to the problem
std::string Problem14::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The number " << maxNum << " produced a chain of " << maxLength << " steps";
	return result.str();
}
//Returns the length of the requested chain
uint64_t Problem14::getLength() const{
	solvedCheck("length of the longest chain");
	return maxLength;
}
//Returns the starting number of the requested chain
uint64_t Problem14::getStartingNumber() const{
	solvedCheck("starting number of the longest chain");
	return maxNum;
}
