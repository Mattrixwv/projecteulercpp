//ProjectEuler/ProjectEulerCPP/src/Problems/Problem21.cpp
//Matthew Ellison
// Created: 11-08-18
//Modified: 07-02-21
//Evaluate the sum of all the amicable numbers under 10000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <vector>
#include <string>
#include <sstream>
#include "mee/numberAlgorithms.hpp"
#include "mee/vectorAlgorithms.hpp"
#include "Problems/Problem21.hpp"


//The top number that will be evaluated
int Problem21::LIMIT = 10000;	//The top number that will be evaluated

//Reserve the size of the vector to speed up insertion
void Problem21::reserveVectors(){
	divisorSum.reserve(LIMIT);	//Reserving it now makes it faster later
	divisorSum.resize(LIMIT);	//Make sure there are enough spaces
}

//Constructor
Problem21::Problem21() : Problem("Evaluate the sum of all the amicable numbers under 10000"){
	reserveVectors();
}

//Solve the problem
void Problem21::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Generate the divisors of all the numbers < 10000, get their sum, and add it to the list
	for(int cnt = 1;cnt < LIMIT;++cnt){
		std::vector<int> divisors = mee::getDivisors(cnt);	//Get all the divisors of a number
		if(divisors.size() > 1){
			divisors.pop_back();	//Remove the last entry because it will be the number itself
		}
		divisorSum[cnt] = mee::getSum(divisors);	//Add the sum of the divisors to the vector
	}
	//Check every sum of divisors in the list for a matching sum
	for(uint64_t cnt = 1;cnt < divisorSum.size();++cnt){
		uint64_t sum = divisorSum[cnt];
		//If the sum is greater than the number of divisors then it is impossible to be amicable. Skip the number and continue
		if(sum >= divisorSum.size()){
			continue;
		}
		//We know that divisorSum[cnt] == sum, so if divisorSum[sum] == cnt we found an amicable number
		if(divisorSum[sum] == cnt){
			//A number can't be amicable with itself
			if(sum == cnt){
				continue;
			}
			//Add the number to the amicable vector
			amicable.push_back(cnt);
		}
	}

	//Sort the vector for neatness
	std::sort(amicable.begin(), amicable.end());


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem21::reset(){
	Problem::reset();
	divisorSum.clear();
	amicable.clear();
	reserveVectors();
}

//Gets
//Return a string with the solution to the problem
std::string Problem21::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "All amicable numbers less than 10000 are\n";
	for(unsigned int cnt = 0;cnt < amicable.size();++cnt){
		result << amicable[cnt] << '\n';
	}
	result << "The sum of all of these amicable numbers is " << mee::getSum(amicable);
	return result.str();
}
//Returns a vector with all of the amicable numbers calculated
std::vector<uint64_t> Problem21::getAmicable() const{
	solvedCheck("amicable numbers");
	return amicable;
}
//Returns the sum of all of the amicable numbers
uint64_t Problem21::getSum() const{
	solvedCheck("sum of the amicable numbers");
	return mee::getSum(amicable);
}
