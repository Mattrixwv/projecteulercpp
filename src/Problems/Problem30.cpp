//ProjectEuler/ProjectEulerCPP/src/Problems/Problem30.cpp
//Matthew Ellison
// Created: 10-27-19
//Modified: 07-02-21
//Find the sum of all the numbers that can be written as the sum of the fifth powers of their digits.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cinttypes>
#include <cmath>
#include <sstream>
#include <string>
#include <vector>
#include "mee/vectorAlgorithms.hpp"
#include "Problems/Problem30.hpp"


//This is the largest number that will be checked
uint64_t Problem30::TOP_NUM = 1000000;
//Start with 2 because 0 and 1 don't count
uint64_t Problem30::BOTTOM_NUM = 2;
//This is the power that the digits are raised to
uint64_t Problem30::POWER_RAISED = 5;

//Returns a vector with the indivitual digits of the number passed into it
std::vector<uint64_t> Problem30::getDigits(uint64_t num){
	std::vector<uint64_t> listOfDigits;	//This vector holds the individual digits of num
	//The easiest way to get the individual digits of a number is by converting it to a string
	std::string digits = std::to_string(num);	//Holds a string representation of num
	//Start with the first digit, convert it to an integer, store it in the vector, and move to the next digit
	for(unsigned int cnt = 0;cnt < digits.size();++cnt){
		listOfDigits.push_back(std::stoi(digits.substr(cnt, 1)));
	}
	//Return the list of digits
	return listOfDigits;
}

//Constructor
Problem30::Problem30() : Problem("Find the sum of all the numbers that can be written as a sum of the fifth powers of their digits"){
}

//Solve the problem
void Problem30::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();

	//Start with the lowest number and increment until you reach the largest number
	for(uint64_t currentNum = BOTTOM_NUM;currentNum <= TOP_NUM;++currentNum){
		//Get the digits of the number
		std::vector<uint64_t> digits = getDigits(currentNum);
		//Get the sum of the powers
		uint64_t sumOfPowers = 0;
		for(uint64_t num : digits){
			sumOfPowers += std::pow(num, POWER_RAISED);
		}
		//Check if the sum of the powers is the same as the number
		//If it is add it to the list, otherwise continue to the next number
		if(sumOfPowers == currentNum){
			sumOfFifthNumbers.push_back(currentNum);
		}
	}

	sum = mee::getSum(sumOfFifthNumbers);

	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem30::reset(){
	Problem::reset();
	sumOfFifthNumbers.clear();
}

//Gets
//Return a string with the solution to the problem
std::string Problem30::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The sum of all the numbers that can be written as the sum of the fifth powers of their digits is " << sum;
	return result.str();
}
//This returns the top number to be checked
uint64_t Problem30::getTopNum() const{
	solvedCheck("largest number checked");
	return TOP_NUM;
}
//This returns a copy of the vector holding all the numbers that are the sum of the fifth power of their digits
std::vector<uint64_t> Problem30::getListOfSumOfFifths() const{
	solvedCheck("list of all numbers that are the sum of the 5th power of their digits");
	return sumOfFifthNumbers;
}
//This returns the sum of all entries in sumOfFifthNumbers
uint64_t Problem30::getSumOfList() const{
	solvedCheck("sum of all numbers that are the sum of the 5th power of their digits");
	return sum;
}
