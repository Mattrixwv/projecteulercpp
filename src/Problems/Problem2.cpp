//ProjectEuler/ProjectEulerCPP/src/Problems/Problem2.cpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//The sum of the even Fibonacci numbers less than 4,000,000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <sstream>
#include <string>
#include "mee/numberAlgorithms.hpp"
#include "Problems/Problem2.hpp"


//Holds the largest number that we are looking for
uint64_t Problem2::TOP_NUM = 4000000 - 1;

//Constructor
Problem2::Problem2() : Problem("What is the sum of the even Fibonacci numbers less than 4,000,000?"), fullSum(0){
}

//Solve the problem
void Problem2::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Get all the Fibonacci numbers <= TOP_NUM
	std::vector<uint64_t> fibList = mee::getAllFib(TOP_NUM);
	//Step through the sum of all the elements to find the even numbers and add them to the list
	for(uint64_t num : fibList){
		//If the number is even add it to the list
		if((num % 2) == 0){
			fullSum += num;
		}
		//Otherwise ignore it
	}


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem2::reset(){
	Problem::reset();
	fullSum = 0;
}

//Gets
//Return a string with the solution to the problem
std::string Problem2::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The sum of the even Fibonacci numbers less than 4,000,000 is " << fullSum;
	return result.str();
}
//Returns the requested sum
uint64_t Problem2::getSum() const{
	solvedCheck("sum");
	return fullSum;
}
