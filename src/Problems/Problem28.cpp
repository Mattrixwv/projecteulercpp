//ProjectEuler/ProjectEulerCPP/src/Problems/Problem28.cpp
//Matthew Ellison
// Created: 09-21-19
//Modified: 07-02-21
//What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed by starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <sstream>
#include <string>
#include <vector>
#include "Problems/Problem28.hpp"


//This sets up the grid to hold the correct number of variables
void Problem28::setupGrid(){
	//Set the size of the grid to 1001 x 1001
	for(int cnt = 0;cnt < 1001;++cnt){
		grid.emplace_back();
		for(int location = 0;location < 1001;++location){
			grid[cnt].push_back(0);
		}
	}
}
//Puts all of the numbers in the grid up the grid
void Problem28::createGrid(){
	bool finalLocation = false;	//A flag to indicate if the final location to be filled has been reached
	//Set the number that is going to be put at each location
	int currentNum = 1;
	//Start with the middle location and set it correctly and advance the tracker to the next number
	int xLocation = 500;
	int yLocation = 500;
	grid[yLocation][xLocation] = currentNum++;
	//Move right the first time
	++xLocation;
	
	//Move in a circular pattern until you reach the final location
	while(!finalLocation){
		//Move down until you reach a blank location on the left
		while(grid[yLocation][xLocation - 1] != 0){
			grid[yLocation][xLocation] = currentNum++;
			++yLocation;
		}

		//Move left until you reach a blank location above
		while(grid[yLocation - 1][xLocation] != 0){
			grid[yLocation][xLocation] = currentNum++;
			--xLocation;
		}

		//Move up until you reach a blank location to the right
		while(grid[yLocation][xLocation + 1] != 0){
			grid[yLocation][xLocation] = currentNum++;
			--yLocation;
		}

		//Move right until you reach a blank location below
		while(grid[yLocation + 1][xLocation] != 0){
			grid[yLocation][xLocation] = currentNum++;
			++xLocation;
			//Check if you are at the final location and break the loop if you are
			if(xLocation == (int)grid.size()){
				finalLocation = true;
				break;
			}
		}
	}
}
//Finds the sum of the diagonals in the grid
void Problem28::findSum(){
	//Start at the top corners and work your way down moving toward the opposite side
	unsigned int leftSide = 0;
	unsigned int rightSide = grid.size() - 1;
	unsigned int row = 0;
	while(row < grid.size()){
		//This ensures the middle location is only counted once
		if(leftSide == rightSide){
			sumOfDiagonals += grid[row][leftSide];
		}
		else{
			sumOfDiagonals += grid[row][leftSide];
			sumOfDiagonals += grid[row][rightSide];
		}
		++row;
		++leftSide;
		--rightSide;
	}
}

//Constructor
Problem28::Problem28() : Problem("What is the sum of the number on the diagonals in a 1001 x 1001 spiral formed by starting with the number 1 and moving to the right in a clockwise direction?"){
	setupGrid();
	sumOfDiagonals = 0;
}

//Solve the problem
void Problem28::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Setup the grid
	createGrid();
	//Find the sum of the diagonals in the grid
	findSum();


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem28::reset(){
	Problem::reset();
	sumOfDiagonals = 0;
	grid.clear();
	setupGrid();
}

//Gets
//Return a string with the solution to the problem
std::string Problem28::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The sum of the diagonals in the given grid is " << sumOfDiagonals;
	return result.str();
}
//Returns the grid
std::vector<std::vector<int>> Problem28::getGrid() const{
	solvedCheck("grid");
	return grid;
}
//Returns the sum of the diagonals
uint64_t Problem28::getSum() const{
	solvedCheck("sum");
	return sumOfDiagonals;
}
