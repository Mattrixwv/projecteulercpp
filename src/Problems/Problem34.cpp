//ProjectEuler/ProjectEulerCPP/src/Problems/Problem34.cpp
//Matthew Ellison
// Created: 06-01-21
//Modified: 07-02-21
//Find the sum of all numbers which are equal to the sum of the factorial of their digits
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <sstream>
#include <string>
#include <vector>
#include "mee/numberAlgorithms.hpp"
#include "Problems/Problem34.hpp"


//The largest num that can be the sum of its own digits
int Problem34::MAX_NUM = 1499999;

//Constructor
Problem34::Problem34() : Problem("Find the sum of all numbers which are equal to the sum of the factorial of their digits"){
	sum = 0;
	factorials.reserve(10);	//Reserve a size of 10 so that factorials[x] = x!
	factorials.resize(10);
}

//Operational functions
//Solve the problem
void Problem34::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Pre-compute the possible factorials from 0! to 9!
	for(int cnt = 0;cnt <= 9;++cnt){
		factorials[cnt] = mee::factorial(cnt);
	}
	//Run through all possible numbers from 3-MAX_NUM and see if they equal the sum of their digit's factorials
	for(int cnt = 3;cnt < MAX_NUM;++cnt){
		//Split the number into its digits and add each one to the sum
		std::string numString = std::to_string(cnt);
		int currentSum = 0;
		for(char number : numString){
			int num = number - 48;
			currentSum += factorials[num];
		}
		//If the number is equal to the sum add the sum to the running sum
		if(currentSum == cnt){
			sum += currentSum;
		}
	}


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem34::reset(){
	Problem::reset();
	sum = 0;
	factorials.clear();
	factorials.reserve(10);
	factorials.resize(10);
}

//Gets
//Return a string with the solution to the problem
std::string Problem34::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The sum of all numbers that are the sum of their digit's factorials is " << sum;
	return result.str();
}
//Returns the list of factorials from 0-9
std::vector<int> Problem34::getFactorials() const{
	solvedCheck("list of factorials");
	return factorials;
}
//Returns the sum of all numbers equal to the sum of their digit's factorials
int Problem34::getSum() const{
	solvedCheck("sum");
	return sum;
}
