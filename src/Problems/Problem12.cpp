//ProjectEuler/ProjectEulerCPP/src/Problems/Problem12.cpp
//Matthew Ellison
// Created: 09-27-18
//Modified: 07-02-21
//What is the value of the first triangle number to have over five hundred divisors?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <sstream>
#include <string>
#include <vector>
#include "mee/numberAlgorithms.hpp"
#include "Problems/Problem12.hpp"


//The number of divisors that you want
uint64_t Problem12::GOAL_DIVISORS = 500;

//Constructor
Problem12::Problem12() : Problem("What is the value of the first triangle number to have over five hundred divisors?"), sum(1), counter(2){
}

//Solve the problem
void Problem12::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Setup your variables
	bool foundNumber = false;	//To flag whether the number has been found

	//Start the timer
	timer.start();


	while(!foundNumber){
		divisors = mee::getDivisors(sum);
		//If the number of divisors is correct set the flag. It must be > 500
		if(divisors.size() > GOAL_DIVISORS){
			foundNumber = true;
		}
		//Otherwise add to the sum and increase the next numeber
		else{
			sum += counter;
			++counter;
		}
	}


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem12::reset(){
	Problem::reset();
	divisors.clear();
	sum = 1;
	counter = 2;
}

//Gets
//Return a string with the solution to the problem
std::string Problem12::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The triangular number " << sum << " is a sum of all numbers >= " << counter - 1 << " and has " << divisors.size() << " divisors";
	return result.str();
}
//Returns the triangular number
int64_t Problem12::getTriangularNumber() const{
	solvedCheck("triangular number");
	return sum;
}
//Get the final number that was added to the triangular number
int64_t Problem12::getLastNumberAdded() const{
	solvedCheck("last number added to get the triangular number");
	return counter - 1;
}
//Returns the list of divisors of the requested number
std::vector<int64_t> Problem12::getDivisorsOfTriangularNumber() const{
	solvedCheck("divisors of the triangular number");
	return divisors;
}
//Returns the number of divisors of the requested number
size_t Problem12::getNumberOfDivisors() const{
	solvedCheck("number of divisors of the triangular number");
	return divisors.size();
}
