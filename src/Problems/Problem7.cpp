//ProjectEuler/ProjectEulerCPP/src/Problems/Problem7.cpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//What is the 10001th prime number?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <sstream>
#include <string>
#include "mee/numberAlgorithms.hpp"
#include "Problems/Problem7.hpp"


//The index of the prime number to find
uint64_t Problem7::NUMBER_OF_PRIMES = 10001;

//Constructor
Problem7::Problem7() : Problem("What is the 10001th prime number?"){
}

//Solve the problem
void Problem7::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Get the correct number of prime numbers
	primes = mee::getNumPrimes(NUMBER_OF_PRIMES);


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem7::reset(){
	Problem::reset();
}

//Gets
//Return a string with the solution to the problem
std::string Problem7::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The " << NUMBER_OF_PRIMES << "th prime number is " << primes[primes.size() - 1];
	return result.str();
}
//Returns the requested prime number
uint64_t Problem7::getPrime() const{
	solvedCheck("prime");
	return *primes.end();
}
