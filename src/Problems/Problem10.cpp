//ProjectEuler/ProjectEulerCPP/src/Problems/Problem10.cpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//Find the sum of all the primes below two million
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <sstream>
#include <string>
#include "mee/numberAlgorithms.hpp"
#include "mee/vectorAlgorithms.hpp"
#include "Problems/Problem10.hpp"


//The largest number to check for primes
uint64_t Problem10::GOAL_NUMBER = 2000000 - 1;	//2,000,000 - 1 because is needs to be < instead of <=

//Constructor
Problem10::Problem10() : Problem("Find the sum of all the primes below two million"), sum(0){
}

//Solve
void Problem10::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Get the sum of all prime numbers <= GOAL_NUMBER
	sum = mee::getSum(mee::getPrimes(GOAL_NUMBER));


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem10::reset(){
	Problem::reset();
	sum = 0;
}

//Gets
//Return a string with the solution to the problem
std::string Problem10::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The sum of all the primes less than " << GOAL_NUMBER + 1 << " is " << sum;
	return result.str();
}
//Returns the sum that was requested
uint64_t Problem10::getSum() const{
	solvedCheck("sum");
	return sum;
}
