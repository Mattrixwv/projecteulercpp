//ProjectEuler/ProjectEulerCPP/src/Problems/Problem1.cpp
//Matthew Ellison
// Created: 07-10-19
//Modified: 07-02-21
//What is the sum of all the multiples of 3 or 5 that are less than 1000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <cmath>
#include <sstream>
#include <string>
#include <vector>
#include "Problems/Problem1.hpp"


//The highest number to be tested
uint64_t Problem1::MAX_NUMBER = 999;

//Constructor
Problem1::Problem1() : Problem("What is the sum of all the multiples of 3 or 5 that are less than 1000?"), fullSum(0){
}

//Gets the sum of the progression of the multiple
uint64_t Problem1::sumOfProgression(uint64_t multiple){
	uint64_t numTerms = std::floor(MAX_NUMBER / multiple);	//This gets the number of multiples of a particular number that is < MAX_NUMBER
	//The sum of progression formula is (n / 2)(a + l). n = number of terms, a = multiple, l = last term
	return ((numTerms / 2.0) * (multiple + (numTerms * multiple)));
}

//Operational functions
//Solve the problem
void Problem1::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Get the sum of the progressions of 3 and 5 and remove the sum of progressions of the overlap
	fullSum = sumOfProgression(3) + sumOfProgression(5) - sumOfProgression(3 * 5);


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem1::reset(){
	Problem::reset();
	fullSum = 0;
}

//Gets
//Return a string with the solution to the problem
std::string Problem1::getResult() const{
	Problem::solvedCheck("result");
	std::stringstream result;
	result << "The sum of all the numbers < " << MAX_NUMBER + 1 << " that are divisible by 3 or 5 is " << fullSum;
	return result.str();
}
//Returns the requested sum
uint64_t Problem1::getSum() const{
	Problem::solvedCheck("sum");
	return fullSum;
}
