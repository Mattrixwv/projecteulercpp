//ProjectEuler/ProjectEulerCPP/src/Problems/Problem35.cpp
//Matthew Ellison
// Created: 06-02-21
//Modified: 07-02-21
//How many circular primes are there below one million?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//TODO: This could possibly be improved by adding all rotations at the same time and removing them from the list
//TODO: This could also be improved by skipping any prime that includes 2, 4, 5, 6, 8, or 0


#include <sstream>
#include <string>
#include <vector>
#include "mee/numberAlgorithms.hpp"
#include "mee/vectorAlgorithms.hpp"
#include "Problems/Problem35.hpp"


//The largest number that we are checking for primes
int Problem35::MAX_NUM = 999999;

//Function
//Returns a list of all rotations of a string passed to it
std::vector<std::string> Problem35::getRotations(std::string str){
	std::vector<std::string> rotations;
	rotations.push_back(str);
	for(unsigned long cnt = 1;cnt < str.size();++cnt){
		str = str.substr(1) + str[0];
		rotations.push_back(str);
	}
	return rotations;
}

//Constructor
Problem35::Problem35() : Problem("How many circular primes are there below one million?"){
}

//Operational functions
//Solve the problem
void Problem35::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Get all primes under 1,000,000
	primes = mee::getPrimes(MAX_NUM);
	//Go through all primes, get all their rotations, and check if those numbers are also primes
	for(int prime : primes){
		bool allRotationsPrime = true;
		//Get all of the rotations of the prime and see if they are also prime
		std::vector<std::string> rotations = getRotations(std::to_string(prime));
		for(std::string rotation : rotations){
			int p = std::stoi(rotation);
			if(!mee::isFound(primes, p)){
				allRotationsPrime = false;
				break;
			}
		}
		//If all rotations are prime add it to the list of circular primes
		if(allRotationsPrime){
			circularPrimes.push_back(prime);
		}
	}


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem35::reset(){
	Problem::reset();
	primes.clear();
	circularPrimes.clear();
}
//Gets
//Returns a string with the solution to the problem
std::string Problem35::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The number of all circular prime numbers under " << MAX_NUM << " is " << circularPrimes.size();
	return result.str();
}
//Returns the vector of primes < MAX_NUM
std::vector<int> Problem35::getPrimes() const{
	solvedCheck("list of primes");
	return primes;
}
//Returns the vector of circular primes < MAX_NUM
std::vector<int> Problem35::getCircularPrimes() const{
	solvedCheck("list of circular primes");
	return circularPrimes;
}
//Returns the number of circular primes
int Problem35::getNumCircularPrimes() const{
	solvedCheck("number of circular primes");
	return circularPrimes.size();
}
