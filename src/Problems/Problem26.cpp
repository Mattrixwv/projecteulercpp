//ProjectEuler/ProjectEulerCPP/src/Problems/Problem26.cpp
//Matthew Ellison
// Created: 07-28-19
//Modified: 07-02-21
//Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <string>
#include <sstream>
#include <vector>
#include "mee/vectorAlgorithms.hpp"
#include "Problems/Problem26.hpp"


//Holds the highest denominator we will check
unsigned int Problem26::TOP_NUMBER = 999;

//Constructor
Problem26::Problem26() : Problem("Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part."), longestCycle(0), longestNumber(1){
	longestCycle = 0;
	longestNumber = 0;
}

//Solve the problem
void Problem26::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Start with 1/2 and find out how long the longest cycle is by checking the remainders
	//Loop through every number from 2-999 and use it for the denominator
	for(unsigned int denominator = 2;denominator <= TOP_NUMBER;++denominator){
		std::vector<unsigned int> remainderList;	//Holds the list of remainders
		bool endFound = false;	//Holds whether we have found an end to the number (either a cycle or a 0 for remainder)
		bool cycleFound = false;	//Holds whether a cycle was detected
		unsigned int numerator = 1;	//The numerator that will be divided
		while(!endFound){
			//Get the remainder after the division
			unsigned int remainder = numerator % denominator;
			//Check if the remainder is 0
			//If it is set the flag
			if(remainder == 0){
				endFound = true;
			}
			//Check if the remainder is in the list
			//If it is in the list, set the appropriate flags
			else if(mee::isFound(remainderList, remainder)){
				endFound = true;
				cycleFound = true;
			}
			//Else add it to the list
			else{
				remainderList.push_back(remainder);
			}
			//Multiply the remainder by 10 to continue finding the next remainder
			numerator = remainder * 10;
		}
		//If a cycle was found check the size of the list against the largest cycle
		if(cycleFound){
			//If it is larger than the largest, set it as the new largest
			if(remainderList.size() > longestCycle){
				longestCycle = remainderList.size();
				longestNumber = denominator;
			}
		}
	}


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem26::reset(){
	Problem::reset();
	longestCycle = 0;
	longestNumber = 1;
}

//Gets
//Return a string with the solution to the problem
std::string Problem26::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The longest cycle is " << longestCycle << " digits long"
		<< "\nIt is started with the number " << longestNumber;
	return result.str();
}
//Returns the length of the longest cycle
unsigned int Problem26::getLongestCycle() const{
	solvedCheck("length of the longest cycle");
	return longestCycle;
}
//Returns the denominator that starts the longest cycle
unsigned int Problem26::getLongestNumber() const{
	solvedCheck("denominator that starts the longest cycle");
	return longestNumber;
}
