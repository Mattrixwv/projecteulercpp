//ProjectEuler/ProjectEulerCPP/src/Problems/Problem8.cpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//Find the thirteen adjacent digits in the 1000-digit number that have the greatest product. What is the value of this product?
/*
73167176531330624919225119674426574742355349194934
96983520312774506326239578318016984801869478851843
85861560789112949495459501737958331952853208805511
12540698747158523863050715693290963295227443043557
66896648950445244523161731856403098711121722383113
62229893423380308135336276614282806444486645238749
30358907296290491560440772390713810515859307960866
70172427121883998797908792274921901699720888093776
65727333001053367881220235421809751254540594752243
52584907711670556013604839586446706324415722155397
53697817977846174064955149290862569321978468622482
83972241375657056057490261407972968652414535100474
82166370484403199890008895243450658541227588666881
16427171479924442928230863465674813919123162824586
17866458359124566529476545682848912883142607690042
24219022671055626321111109370544217506941658960408
07198403850962455444362981230987879927244284909188
84580156166097919133875499200524063689912560717606
05886116467109405077541002256983155200055935729725
71636269561882670428252483600823257530420752963450
*/
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <sstream>
#include <string>
#include <vector>
#include "Problems/Problem8.hpp"


//The number that we are working with
std::string Problem8::number = "7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450";

//Constructor
Problem8::Problem8() : Problem("Find the thirteen adjacent digits in the 1000-digit number that have the greatest product. What is the value of this product?"), maxProduct(0){
}

//Solve the problem
void Problem8::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Cycle through the string of numbers looking for the maximum product
	for(unsigned int cnt = 12;cnt < number.size();++cnt){
		uint64_t currentProduct = std::stoull(number.substr(cnt, 1)) * std::stoull(number.substr(cnt - 1, 1)) * std::stoull(number.substr(cnt - 2, 1)) * std::stoull(number.substr(cnt - 3, 1)) * std::stoull(number.substr(cnt - 4, 1)) * std::stoull(number.substr(cnt - 5, 1)) * std::stoull(number.substr(cnt - 6, 1)) * std::stoull(number.substr(cnt - 7, 1)) * std::stoull(number.substr(cnt - 8, 1)) * std::stoull(number.substr(cnt - 9, 1)) * std::stoull(number.substr(cnt - 10, 1)) * std::stoull(number.substr(cnt - 11, 1)) * std::stoull(number.substr(cnt - 12, 1));

		//Check if the product is greater than the current maximum
		//If it is currentNums is the new maximum
		if(currentProduct > maxProduct){
			maxNums = number.substr(cnt - 12, 13);
			maxProduct = currentProduct;
		}
	}


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run
void Problem8::reset(){
	Problem::reset();
	maxNums = "";
	maxProduct = 0;
}

//Gets
//Return a string with the solution to the problem
std::string Problem8::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The greatest product is " << maxProduct
			<< "\nThe numbers are " << maxNums;
	return result.str();
}
//Returns the string of numbers that produces the largest product
std::string Problem8::getLargestNums() const{
	solvedCheck("numbers that make the largest product");
	return maxNums;
}
//Returns the requested product
uint64_t Problem8::getLargestProduct() const{
	solvedCheck("product of the numbers");
	return maxProduct;
}
