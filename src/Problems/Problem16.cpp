//ProjectEuler/ProjectEulerCPP/src/Problems/Problem16.cpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//What is the sum of the digits of the number 2^1000?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
//This file contains a header from the gmp library. The library is used for large integers.
//You can find more information about them at https://gmplib.org/
//When compiling this file you need to have the gmp library installed as well as linking the libraries to your executable using the -lgmpxx and -lgmp flags
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <sstream>
#include <string>
#include <gmpxx.h>
#include "Problems/Problem16.hpp"


int Problem16::NUM_TO_POWER = 2;	//The number that is going to be raised to a power
int Problem16::POWER = 1000;	//The power that the number is going to be raised to

//Constructor
Problem16::Problem16() : Problem("What is the sum of the digits of the number 2^1000?"), num(0), sumOfElements(0){
}

//Solve the problem
void Problem16::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Get the number
	mpz_ui_pow_ui(num.get_mpz_t(), NUM_TO_POWER, POWER);
	//Get a string of the number
	std::string numString = num.get_str();
	//Add up the individual characters of the string
	for(char number : numString){
		std::string tempString;
		tempString += number;
		sumOfElements += std::stoi(tempString);
	}


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem16::reset(){
	Problem::reset();
	num = 0;
	sumOfElements = 0;
}

//Gets
//Return a string with the solution to the problem
std::string Problem16::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << NUM_TO_POWER << '^' << POWER << " = " << num
			<< "\nThe sum of the elements is " << sumOfElements;
	return result.str();
}
//Returns the number that was calculated
mpz_class Problem16::getNumber() const{
	solvedCheck("number");
	return num;
}
//Return the sum of the digits of the number
int Problem16::getSum() const{
	solvedCheck("sum");
	return sumOfElements;
}
