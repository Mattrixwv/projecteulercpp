//ProjectEuler/ProjectEulerCPP/src/Problems/Problem32.cpp
//Matthew Ellison
// Created: 07-27-20
//Modified: 07-02-21
//Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <sstream>
#include <string>
#include <vector>
#include "mee/stringAlgorithms.hpp"
#include "Problems/Problem32.hpp"


int Problem32::TOP_MULTIPLICAND = 99;	//The largest multiplicand to check
int Problem32::TOP_MULTIPLIER = 4999;	//The largest multiplier to check

//Returns true if the passed productset is 1-9 pandigital
bool Problem32::isPandigital(ProductSet currentSet){
	//Get the numbers out of the object and put them into a string
	std::string numberString = currentSet.getNumString();
	//Make srue the string is the correct length
	if(numberString.size() != 9){
		return false;
	}
	//Make sure every number from 1-9 is contained exactly once
	for(int panNumber = 1;panNumber <= 9;++panNumber){
		//Make sure there is exactly one of this number contained in the string
		if(mee::findNumOccurrence(numberString, std::to_string(panNumber)[0]) != 1){
			return false;
		}
	}
	//If all numbers were found in the string return true
	return true;
}

//Constructor
Problem32::Problem32() : Problem("Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital."), sumOfPandigitals(0){
}

//Operational functions
//Solve the problem
void Problem32::solve(){
	//If the problem has alread been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Create the multiplicand and start working your way up
	for(int multiplicand = 1;multiplicand <= TOP_MULTIPLICAND;++multiplicand){
		//Run through all possible multipliers
		for(int multiplier = multiplicand;multiplier <= TOP_MULTIPLIER;++multiplier){
			ProductSet currentProductSet(multiplicand, multiplier);
			//If the product is too long move on to the next possible number
			if(currentProductSet.getNumString().size() > 9){
				break;
			}
			//If the current number is a pandigital that doesn't already exist in the list add it to the list
			if(isPandigital(currentProductSet)){
				if(std::find(listOfProducts.begin(), listOfProducts.end(), currentProductSet) == listOfProducts.end()){
					listOfProducts.push_back(currentProductSet);
				}
			}
		}
	}

	//Get the sum of the products of the pandigitals
	for(ProductSet prod : listOfProducts){
		sumOfPandigitals += prod.getProduct();
	}


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem32::reset(){
	Problem::reset();
	listOfProducts.clear();
	sumOfPandigitals = 0;
}

//Gets
//Return a string with the solution to the problem
std::string Problem32::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "There are " << listOfProducts.size() << " unique 1-9 pandigitals\nThe sum of the products of these pandigitals is " << sumOfPandigitals;
	return result.str();
}
//Returns the sum of the pandigitals
int64_t Problem32::getSumOfPandigitals() const{
	solvedCheck("sum of the pandigitals");
	return sumOfPandigitals;
}