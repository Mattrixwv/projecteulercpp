//ProjectEuler/ProjectEulerCPP/src/Problems/Problem20.cpp
//Matthew Ellison
// Created: 11-07-18
//Modified: 07-02-21
//What is the sum of the digits of 100!?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
//This file contains a header from the gmp library. The library is used for large integers.
//You can find more information about them at https://gmplib.org/
//When compiling this file you need to have the gmp library installed as well as linking the libraries to your executable using the -lgmpxx and -lgmp flags
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <sstream>
#include <string>
#include "gmpxx.h"
#include "Problems/Problem20.hpp"


//Constructor
Problem20::Problem20() : Problem("What is the sum of the digits of 100!?"), num(1), sum(0){
	//num Starts at 1 because multiplication is performed on it
	//sum starts at 0 because addition is performed on it
}

//Solve the problem
void Problem20::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Run through every number from 1 to 100 and multiply it by the current num
	for(int cnt = 1;cnt <= 100;++cnt){
		num *= cnt;
	}

	//Get a string of the number because it is easier to pull appart the individual characters
	std::string numString = num.get_str();
	//Run through every character in the string, convert it back to an integer and add it to the running sum
	for(unsigned int cnt = 0;cnt < numString.size();++cnt){
		sum += std::stoi(numString.substr(cnt, 1));
	}


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem20::reset(){
	Problem::reset();
	sum = 0;
	num = 1;
}

//Gets
//Return a string with the solution to the problem
std::string Problem20::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "100! = " << num.get_str()
			<< "\nThe sum of the digits is: " << sum;
	return result.str();
}
//Returns the number 100!
mpz_class Problem20::getNumber() const{
	solvedCheck("number");
	return num;
}
//Returns the number 100! in a string
std::string Problem20::getNumberString() const{
	solvedCheck("number as a string");
	return num.get_str();
}
//Returns the sum of the digits of 100!
uint64_t Problem20::getSum() const{
	solvedCheck("sum of the digits");
	return sum;
}
