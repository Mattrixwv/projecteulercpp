//ProjectEuler/ProjectEulerCPP/src/Problems/Problem36.cpp
//Matthew Ellison
// Created: 06-29-21
//Modified: 07-02-21
//Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <sstream>
#include <string>
#include <vector>
#include "mee/numberAlgorithms.hpp"
#include "mee/stringAlgorithms.hpp"
#include "mee/vectorAlgorithms.hpp"
#include "Problems/Problem36.hpp"


//The largest number that will be checked
int Problem36::MAX_NUM = 999999;

//Constructor
Problem36::Problem36() : Problem("Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2."), sum(0){
}

//Operational functions
//Solve the problem
void Problem36::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Start with 1, check if it is a palindrome in base 10 and 2, and continue to MAX_NUM
	for(int num = 1;num < MAX_NUM;++num){
		//Check if num is a palindrome
		if(mee::isPalindrome(std::to_string(num))){
			//Convert num to base 2 and see if that is a palindrome
			std::string binNum = mee::toBin(num);
			if(mee::isPalindrome(binNum)){
				//Add num to the vector of palindromes
				palindromes.push_back(num);
			}
		}
	}
	//Get the sum of all palindromes in the vector
	sum = mee::getSum(palindromes);


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem36::reset(){
	Problem::reset();
	palindromes.clear();
	sum = 0;
}

//Gets
//Returns a string with the solution to the problem
std::string Problem36::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The sum of all base 10 and base 2 palindromic numbers < " << MAX_NUM << " is " << sum;
	return result.str();
}
//Return the vector of palindromes < MAX_NUM
std::vector<int> Problem36::getPalindromes() const{
	solvedCheck("list of palindromes");
	return palindromes;
}
//Return the sum of all elements in the vector of palindromes
int Problem36::getSumOfPalindromes() const{
	solvedCheck("sum of all palindromes");
	return sum;
}