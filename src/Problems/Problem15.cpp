//ProjectEuler/ProjectEulerCPP/src/Problems/Problem15.hpp
//Matthew Ellison
// Created: 09-29-18
//Modified: 07-02-21
//How many routes from the top left corner to the bottom right corner are there through a 20×20 grid if you can only move right and down?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <sstream>
#include <string>
#include "Problems/Problem15.hpp"


int Problem15::WIDTH = 20;	//The width of the grid
int Problem15::LENGTH = 20;	//The length of the grid

//This function acts as a handler for moving the position on the grid and counting the distance
//It moves right first, then down
void Problem15::move(int currentX, int currentY){
	//Check if you are at the end
	if((currentX == WIDTH) && (currentY == LENGTH)){
		++numOfRoutes;
		return;
	}

	//Move right if possible
	if(currentX < WIDTH){
		move(currentX + 1, currentY);
	}

	//Move down if possible
	if(currentY < LENGTH){
		move(currentX, currentY + 1);
	}
}

//Constructor
Problem15::Problem15() : Problem("How many routes from the top left corner to the bottom right corner are there through a 20×20 grid if you can only move right and down?"), numOfRoutes(0){
}

//Solve the problem
void Problem15::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Setup the variables
	int currentX = 0;	//The current x location on the grid
	int currentY = 0;	//The current y location on the grid

	//Start the timer
	timer.start();


	//We write this as a recursive function
	//When in a location it always moves right first, then down
	move(currentX, currentY);


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem15::reset(){
	Problem::reset();
	numOfRoutes = 0;
}

//Gets
//Return a string with the solution to the problem
std::string Problem15::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The number of routes is " << numOfRoutes;
	return result.str();
}
//Returns the number of routes found
uint64_t Problem15::getNumberOfRoutes() const{
	solvedCheck("number of routes");
	return numOfRoutes;
}
