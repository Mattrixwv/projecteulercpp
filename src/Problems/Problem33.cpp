//ProjectEuler/ProjectEulerCPP/src/Problems/Problem33.cpp
//Matthew Ellison
// Created: 02-05-21
//Modified: 07-02-21
/*
The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s
We shall consider fractions like, 30/50 = 3/5, to be trivial examples
There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator
If the product of these four fractions is given in its lowest common terms, find the value of the denominator
*/
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cinttypes>
#include <numeric>
#include <sstream>
#include <string>
#include <vector>
#include "mee/vectorAlgorithms.hpp"
#include "Problems/Problem33.hpp"


//The lowest the numerator can be
int Problem33::MIN_NUMERATOR = 10;
//The highest the numerator can be
int Problem33::MAX_NUMERATOR = 98;
//The lowest the denominator can be
int Problem33::MIN_DENOMINATOR = 11;
//The highest the denominator can be
int Problem33::MAX_DENOMINATOR = 99;

//Constructor
Problem33::Problem33() : Problem("If the product of these four fractions is given in its lowest common terms, find the value of the denominator"){
	prodDenominator = 1;
}

//Operational functions
//Solve the problem
void Problem33::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}
	//Start the timer
	timer.start();


	//Search every possible numerator/denominator pair
	for(int denominator = MIN_DENOMINATOR;denominator <= MAX_DENOMINATOR;++denominator){
		for(int numerator = MIN_NUMERATOR;(numerator < denominator) && (numerator <= MAX_NUMERATOR);++numerator){
			std::string denom = std::to_string(denominator);
			std::string num = std::to_string(numerator);
			int tempNum = 0;
			int tempDenom = 1;

			//Check that this isn't a trivial example
			if((num[1] == '0') && (denom[1] == '0')){
				continue;
			}
			//Remove the offending digits if they exist
			else if(num[0] == denom[0]){
				tempNum = num[1] - 48;
				tempDenom = denom[1] - 48;
			}
			else if(num[0] == denom[1]){
				tempNum = num[1] - 48;
				tempDenom = denom[0] - 48;
			}
			else if(num[1] == denom[0]){
				tempNum = num[0] - 48;
				tempDenom = denom[1] - 48;
			}
			else if(num[1] == denom[1]){
				tempNum = num[0] - 48;
				tempDenom = denom[0] - 48;
			}

			//Test if the new fraction is the same as the old one
			if(((double)tempNum / (double)tempDenom) == ((double)numerator / (double)denominator)){
				numerators.push_back(numerator);
				denominators.push_back(denominator);
			}
		}
	}

	//Get the product of the numbers
	int numProd = mee::getProduct(numerators);
	int denomProd = mee::getProduct(denominators);
	//Get the gcd to reduce to lowest terms
	int gcd = std::gcd(numProd, denomProd);
	//Save the denominator
	prodDenominator = denomProd / gcd;


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem33::reset(){
	Problem::reset();
	numerators.clear();
	denominators.clear();
}

//Gets
//Return a string with the solution to the problem
std::string Problem33::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The denominator of the product is " << prodDenominator;
	return result.str();
}
//Returns the list of numerators
std::vector<int> Problem33::getNumerators() const{
	solvedCheck("list of numerators");
	return numerators;
}
//Returns the list of denominators
std::vector<int> Problem33::getDenominators() const{
	solvedCheck("list of denominators");
	return denominators;
}
//Returns the answer to the question
int Problem33::getProdDenominator() const{
	solvedCheck("denominator");
	return prodDenominator;
}
