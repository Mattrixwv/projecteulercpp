//ProjectEuler/ProjectEulerCPP/src/Problems/Problem9.cpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find the product of abc.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <cmath>
#include <sstream>
#include <string>
#include "Problems/Problem9.hpp"


//a + b + c should equal this number
int Problem9::GOAL_SUM = 1000;

//Constructor
Problem9::Problem9() : Problem("There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find the product of abc."), a(1), b(0), c(0), found(false){
}

//Solve the problem
void Problem9::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Loop through all possible a's
	while((a < GOAL_SUM) && !found){
		b = a + 1;	//b bust be greater than a
		c = std::sqrt((a * a) + (b * b));

		//Loop through all possible b's
		while((a + b + c) < GOAL_SUM){
			++b;
			c = std::sqrt((a * a) + (b * b));
		}

		//If the sum == 1000 you found the number, otherwise go to the next possible a
		if((a + b + c) == GOAL_SUM){
			found = true;
		}
		else{
			++a;
		}
	}


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	if(found){
		solved = true;
	}
	else{
		throw Unsolved("The triplet was not found!");
	}
}
//Reset the problem so it can be run again
void Problem9::reset(){
	Problem::reset();
	a = 1;
	b = 0;
	c = 0;
	found = false;
}

//Gets
//Return a string with the solution to the problem
std::string Problem9::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The Pythagorean triplet is " << a << ' ' << b << ' ' << (int)c
				<< "\nThe numbers' product is " << a * b * (int)c;
	return result.str();
}
//Returns the length of the first side
int Problem9::getSideA() const{
	solvedCheck("first side");
	return a;
}
//Returns the length of the second side
int Problem9::getSideB() const{
	solvedCheck("second side");
	return b;
}
//Returns the length of the hyp
int Problem9::getSideC() const{
	solvedCheck("third side");
	return (int)c;
}
//Returns the product of the 3 sides
int Problem9::getProduct() const{
	solvedCheck("product of all three sides");
	return a * b * (int)c;
}
