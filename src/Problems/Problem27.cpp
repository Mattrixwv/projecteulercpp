//ProjectEuler/ProjectEulerCPP/src/Problems/Problem27.cpp
//Matthew Ellison
// Created: 09-14-19
//Modified: 07-03-21
//Find the product of the coefficients, |a| < 1000 and |b| <= 1000, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n=0.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <string>
#include <sstream>
#include <cinttypes>
#include "mee/numberAlgorithms.hpp"
#include "Problems/Problem27.hpp"


//Constructor
Problem27::Problem27() : Problem("Considering quadratics of the form n^2 + an + b, where |a| < 1000 and |b| <= 1000, find the product of the coefficients a and b that produce the maximum number of primes for consecutive values of n starting with n = 0."){
	topA = topB = topN = 0;
}

//Solve the problem
void Problem27::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Start with the lowest possible A and check all possibilities after that
	for(int64_t a = -999;a <= 999;++a){
		//Start with the lowest possible B and check all possibilities after that
		for(int64_t b = -1000;b <= 1000;++b){
			//Start with n=0 and check the formula to see how many primes you can get with concecutive n's
			int64_t n = 0;
			int64_t quadratic = (n * n) + (a * n) + b;
			while(mee::isPrime(quadratic)){
				++n;
				quadratic = (n * n) + (a * n) + b;
			}
			--n;	//Negate an n because the last formula failed

			//Set all the largest numbers if this created more primes than any other
			if(n > topN){
				topN = n;
				topB = b;
				topA = a;
			}
		}
	}


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem27::reset(){
	Problem::reset();
	topA = topB = topN = 0;
	primes.clear();
}

//Gets
//Return a string with the solution to the problem
std::string Problem27::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The greatest number of primes found is " << topN
		<< "\nIt was found with A = " << topA << ", B = " << topB
		<< "\nThe product of A and B is " << topA * topB;
	return result.str();
}
//Returns the top A that was generated
int64_t Problem27::getTopA() const{
	solvedCheck("largest A");
	return topA;
}
//Returns the top B that was generated
int64_t Problem27::getTopB() const{
	solvedCheck("largest B");
	return topB;
}
//Returns the top N that was generated
int64_t Problem27::getTopN() const{
	solvedCheck("largest N");
	return topN;
}
//Returns the product of A and B for the answer
int64_t Problem27::getProduct() const{
	solvedCheck("product of A and B");
	return topA * topB;
}
