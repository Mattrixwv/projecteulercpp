//ProjectEuler/ProjectEulerCPP/src/Problems/Problem22.cpp
//Matthew Ellison
// Created: 11-09-18
//Modified: 07-02-21
//What is the total of all the name scores in the file?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <algorithm>
#include <cinttypes>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "mee/vectorAlgorithms.hpp"
#include "Problems/Problem22.hpp"


//Take the names from the file and insert them into the vector
void Problem22::getNames(){
	std::ifstream inFile("files/problem22Names.txt");
	if(!inFile.is_open()){
		inFile.open("../files/problem22Names.txt");
	}
	while(!inFile.eof()){
		std::string name;
		inFile >> name;
		names.push_back(name);
	}
	inFile.close();
}

//Reserve the size of the vector to speed up insertion
void Problem22::reserveVectors(){
	//Make sure the vector is the right size
	sums.reserve(names.size());
	sums.resize(names.size());
	//Make sure the vector is the right size
	prod.reserve(names.size());
	prod.resize(names.size());
}

//Constructor
Problem22::Problem22() : Problem("What is the total of all the name scores in the file?"){
	getNames();
	reserveVectors();
}

//Solve the problem
void Problem22::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Sort all the names
	std::sort(names.begin(), names.end());
	//Step through every name
	for(unsigned int nameCnt = 0;nameCnt < names.size();++nameCnt){
		//Step through every character in the current name
		for(unsigned int charCnt = 0;charCnt < names[nameCnt].size();++charCnt){
			//A = 65 so subtracting 64 means A = 1. This will only work correctly if all letters are capitalized
			sums[nameCnt] += (names[nameCnt][charCnt] - 64);
		}
	}
	//Get the product for all numbers
	for(unsigned int cnt = 0;cnt < sums.size();++cnt){
		prod[cnt] = sums[cnt] * (cnt + 1);
	}
	//Get the sum of the product of all numbers
	sum = mee::getSum(prod);


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem22::reset(){
	Problem::reset();
	sums.clear();
	prod.clear();
	reserveVectors();
}


//Gets
//Return a string with the solution to the problem
std::string Problem22::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The answer to the question is " << sum;
	return result.str();
}
//Returns the vector of the names being scored
std::vector<std::string> Problem22::getNames() const{
	solvedCheck("names");
	return names;
}
//Returns the sum of the names' scores
uint64_t Problem22::getNameScoreSum() const{
	solvedCheck("score of the names");
	return mee::getSum(prod);
}
