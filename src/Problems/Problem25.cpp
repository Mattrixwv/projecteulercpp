//ProjectEuler/ProjectEulerCPP/src/Problems/Problem25.cpp
//Matthew Ellison
// Created: 11-13-18
//Modified: 07-02-21
//What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
//This file contains a header from the gmp library. The library is used for large integers.
//You can find more information about them at https://gmplib.org/
//When compiling this file you need to have the gmp library installed as well as linking the libraries to your executable using the -lgmpxx and -lgmp flags
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <sstream>
#include <string>
#include <gmpxx.h>
#include "mee/numberAlgorithms.hpp"
#include "Problems/Problem25.hpp"


//The number of digits to calculate up to
unsigned int Problem25::NUM_DIGITS = 1000;	//The number of digits to calculate up to

//Constructor
Problem25::Problem25() : Problem("What is the index of the first term in the Fibonacci sequence to contain 1000 digits?"), number(0), index(2){
	number = 0;
	index = 0;
}

//Solve the problem
void Problem25::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Move through all Fibonacci numbers until you reach the one with at least NUM_DIGITS digits
	while(number.get_str().size() < NUM_DIGITS){
		++index;	//Increase the index number. Doing this at the beginning keeps the index correct at the end of the loop
		number = mee::getFib(index);	//Calculate the number
	}


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem25::reset(){
	Problem::reset();
	number = 0;
	index = 2;
}

//Gets
//Return a string with the solution to the problem
std::string Problem25::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The first Fibonacci number with " << NUM_DIGITS << " digits is " << number
			<< "\nIts index is " << index;
	return result.str();
}
//Returns the Fibonacci number asked for
mpz_class Problem25::getNumber() const{
	solvedCheck("fibonacci number");
	return number;
}
//Returns the Fibonacci number asked for as a string
std::string Problem25::getNumberString() const{
	solvedCheck("fibonacci number as a string");
	return number.get_str();
}
//Returns the index of the requested Fibonacci number
mpz_class Problem25::getIndex() const{
	solvedCheck("index of the fibonacci number");
	return index;
}
//Returns the index of the requested Fibonacci number as a string
std::string Problem25::getIndexString() const{
	solvedCheck("index of the fibonacci number as a string");
	return index.get_str();
}
//Returns the index of the requested Fibonacci number as a uint64_t
uint64_t Problem25::getIndexInt() const{
	solvedCheck("index of the fibonacci number as an int");
	return index.get_ui();
}
