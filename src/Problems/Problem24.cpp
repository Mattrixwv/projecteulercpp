//ProjectEuler/ProjectEulerCPP/src/Problems/Problem24.cpp
//Matthew Ellison
// Created: 11-11-18
//Modified: 07-02-21
//What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <string>
#include <vector>
#include <sstream>
#include "mee/stringAlgorithms.hpp"
#include "Problems/Problem24.hpp"


//The number of the permutation that you need
int Problem24::NEEDED_PERM = 1000000;
//All of the characters that we need to get the permutations of
std::string Problem24::nums = "0123456789";

//Constructor
Problem24::Problem24() : Problem("What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?"){
}

//Solve the problem
void Problem24::solve(){
	//If the problem has already been solved do nothing and end the function
	if(solved){
		return;
	}

	//Start the timer
	timer.start();


	//Get all permutations of the string
	permutations = mee::getPermutations(nums);


	//Stop the timer
	timer.stop();

	//Throw a flag to show the problem is solved
	solved = true;
}
//Reset the problem so it can be run again
void Problem24::reset(){
	Problem::reset();
	permutations.clear();
}

//Gets
//Return a string with the solution to the problem
std::string Problem24::getResult() const{
	solvedCheck("result");
	std::stringstream result;
	result << "The 1 millionth permutation is " << permutations[NEEDED_PERM - 1];
	return result.str();
}
//Returns a vector with all of the permutations
std::vector<std::string> Problem24::getPermutationsList() const{
	solvedCheck("permutations");
	return permutations;
}
//Returns the specific permutations you are looking for
std::string Problem24::getPermutation() const{
	solvedCheck("1,000,000th permutation");
	return permutations[NEEDED_PERM - 1];
}
