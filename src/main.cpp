//ProjectEuler/ProjectEulerCPP/main.cpp
//Matthew Ellison
// Created: 07-04-19
//Modified: 07-01-21
//This is a driver function for all Project Euler problems
//It prompts the user for an action (state the problem or solve the problem), then executes the appropriate command on the appropriate problem
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <vector>
#include "benchmark.hpp"
#include "Problem.hpp"
#include "ProblemSelection.hpp"


//Some helper functions to help with the menus
void printMenu();	//Prints the menu in the terminal
int getMenuSelection(); //Returns a valid menu selection from the user
bool isValidMenu(int selection);	//Returns true if the int passed into it is a valid menu selection
void solveMenu();	//Prints the menu for solving a problem and handles how the problem is handed off
Problem* getProblem(unsigned int problemNumber);	//Takes a problem's number and returns a pointer to the appropriate class
void solveProblem(Problem* problem);	//Handles how the program solves the problem
void descriptionMenu();	//Handles how the program reacts to printing the problem's description
void printDescription(Problem* problem);	//Prints out the description of the problem passed to it
unsigned int getProblemNumber();	//A helper function to error check the problem number input for solving and describing problems
void listProblems();	//Lists the problem numbers that you can choose

//Setup the menu options
enum MenuOptions {SOLVE = 1, DESCRIPTION, LIST, BENCHMARK, EXIT, SIZE};

int main(){
	int selection = 0;	//Holds the menu selection of the user
	do{
		//Print the menu and prompt the user to select an action
		printMenu();
		selection = getMenuSelection();

		switch(selection){
			case MenuOptions::SOLVE : solveMenu(); break;
			case MenuOptions::DESCRIPTION : descriptionMenu(); break;
			case MenuOptions::LIST : listProblems(); break;
			case MenuOptions::BENCHMARK : benchmarkMenu(); break;
			case MenuOptions::EXIT : break;
		}
	}while(selection != EXIT);

	return 0;
}

void printMenu(){
	std::cout << "1. Solve a problem\n"
			  << "2. Print a problem description\n"
			  << "3. List valid problem numbers\n"
			  << "4. Benchmark\n"
			  << "5. Exit" << std::endl;
}

int getMenuSelection(){
	int selection = 0;
	std::cin >> selection;
	while(std::cin.fail() || !isValidMenu(selection)){
		std::cout << "That is an invalid option!\nPress Enter to continue" << std::endl;
		std::cin.clear();
		std::cin.get();
		printMenu();
		std::cin >> selection;
	}
	return selection;
}

bool isValidMenu(int selection){
	if((selection > 0) && (selection < MenuOptions::SIZE)){
		return true;
	}
	else{
		return false;
	}
}

void solveMenu(){
	unsigned int problemNumber = getProblemNumber();
	Problem* problem = nullptr;
	//This selection solves all problems in order
	if(problemNumber == 0){
		//Solve to every valid problem number, skipping over 0
		for(unsigned int problemLocation = PROBLEM_NUMBERS[1];problemLocation < PROBLEM_NUMBERS.size();++problemLocation){
			//Generate the current problem
			problem = getProblem(PROBLEM_NUMBERS[problemLocation]);
			//Solve the problem
			std::cout << PROBLEM_NUMBERS[problemLocation] << ". ";
			solveProblem(problem);
			//Release the memory
			delete problem;
			//Safeguard
			problem = nullptr;
		}
	}
	//This is if a single problem number was chosen
	else{
		//Generate the problem
		problem = getProblem(problemNumber);
		//Solve the problem
		solveProblem(problem);
		//Release the memory
		delete problem;
		//Just an overprotective safeguard
		problem = nullptr;
	}
}

void descriptionMenu(){
	Problem* problem = nullptr;	//Holds the problem that will be generated
	std::cout << "\n\n";	//Give some extra space to print the description

	//Get the problem number
	unsigned int problemNumber = getProblemNumber();

	//If the problem number is 0 print out all descriptions
	if(problemNumber == 0){
		//Print description for every valid problem number, skipping over 0
		for(unsigned int problemLocation = PROBLEM_NUMBERS[1];problemLocation < PROBLEM_NUMBERS.size();++problemLocation){
			//Generate the problem
			problem = getProblem(PROBLEM_NUMBERS[problemLocation]);
			//Print the problem's description
			std::cout << PROBLEM_NUMBERS[problemLocation] << ". ";
			printDescription(problem);
			std::cout << '\n';
			//Release the memory
			delete problem;
			//Safeguard
			problem = nullptr;
		}
	}
	//Otherwise print out a single problem's description
	else{
		//Generate the problem
		problem = getProblem(problemNumber);
		//Print the problem's description
		printDescription(problem);
		//Release the memory
		delete problem;
		//Safeguard
		problem = nullptr;
	}
}
