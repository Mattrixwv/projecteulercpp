//ProjectEuler/ProjectEulerCPP/headers/Problem.hpp
//Matthew Ellison
// Created: 07-04-19
//Modified: 07-02-21
//This is an abstract base class to allow polymorphism for the individual problems
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM_HPP
#define PROBLEM_HPP


#include <sstream>
#include <string>
#include "mee/Stopwatch.hpp"
#include "Unsolved.hpp"


class Problem{
protected:
	const std::string description;	//Holds the description of the problem
	mee::Stopwatch timer;	//Used to determine your algorithm's run time
	bool solved;	//Holds true after the problem has been solved
	//Make sure the problem has been solved and throw an exception if not
	void solvedCheck(std::string str) const{
		if(!solved){
			throw new Unsolved("You must solve the problem before you can see the " + str);
		}
	}
public:
	//Constructors
	Problem() : solved(false){
	}
	Problem(std::string description) : description(description), solved(false){
	}
	//Destructor
	virtual ~Problem(){
	}
	//Gets
	virtual std::string getDescription() const{
		return description;
	}
	//Get the time it took to run the algorithm as a string
	virtual std::string getTime(){
		solvedCheck("time it took to run the algorithm");
		return timer.getStr();
	}
	//Get a copy of the timer that was used to time the algorithm
	virtual mee::Stopwatch getTimer(){
		solvedCheck("timer");
		return timer;
	}
	//Reset the problem so it can be run again
	virtual void reset(){
		timer.reset();
		solved = false;
	}
	//Pure virtual functions
	//Solve the problem
	virtual void solve() = 0;
	//Return a string with the solution to the problem
	virtual std::string getResult() const = 0;
};

#endif //PROBLEM_HPP
