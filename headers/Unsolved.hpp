//ProjectEuler/ProjectEulerCPP/headers/Unsolved.hpp
//Matthew Ellison
// Created: 08-28-20
//Modified: 07-01-21
//An exception class thrown if you try to access something before it has been solved
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef UNSOLVED_HPP
#define UNSOLVED_HPP


#include <string>


class Unsolved{
private:
	std::string message;
public:
	Unsolved(){}
	Unsolved(std::string message) : message(message){}
	std::string getMessage(){
		return message;
	}
};


#endif //UNSOLVED_HPP
