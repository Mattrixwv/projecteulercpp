//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem21.hpp
//Matthew Ellison
// Created: 11-08-18
//Modified: 07-02-21
//Evaluate the sum of all the amicable numbers under 10000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM21_HPP
#define PROBLEM21_HPP


#include <cinttypes>
#include <string>
#include <vector>
#include "Problem.hpp"


class Problem21 : public Problem{
private:
	//Variables
	//Static variables
	static int LIMIT;	//The top number that will be evaluated
	//Instance variables
	std::vector<uint64_t> divisorSum;	//Holds the sum of the divisors of the subscript number
	std::vector<uint64_t> amicable;		//Holds all amicable numbers

	//Functions
	void reserveVectors();	//Reserve the size of the vector to speed up insertion
public:
	//Constructor
	Problem21();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	std::vector<uint64_t> getAmicable() const;	//Returns a vector with all of the amicable numbers calculated
	uint64_t getSum() const;	//Returns the sum of all of the amicable numbers
};


/* Results:
All amicable numbers less than 10000 are
220
284
1184
1210
2620
2924
5020
5564
6232
6368
The sum of all of these amicable numbers is 31626
It took an average of 4.310 milliseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM21_HPP
