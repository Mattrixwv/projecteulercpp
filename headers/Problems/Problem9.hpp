//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem9.hpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find the product of abc.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM9_HPP
#define PROBLEM9_HPP


#include <string>
#include "Problem.hpp"


class Problem9 : public Problem{
private:
	//Variables
	//Static variables
	static int GOAL_SUM;	//a + b + c should equal this number
	//Instance variables
	int a;		//Holds the size of the first side
	int b;		//Holds the size of the second side
	double c;	//Holds the size of the hyp
	bool found;	//A flag to determine if we have found the solution yet
public:
	//Constructor
	Problem9();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	int getSideA() const;	//Returns the length of the first side
	int getSideB() const;	//Returns the length of the second side
	int getSideC() const;	//Returns the length of the hyp
	int getProduct() const;	//Returns the product of the 3 sides
};


/* Results:
The Pythagorean triplet is 200 375 425
The numbers' product is 31875000
It took an average of 154.595 microseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM9_HPP
