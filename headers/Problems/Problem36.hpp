//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem36.hpp
//Matthew Ellison
// Created: 06-29-21
//Modified: 07-02-21
//Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM36_HPP
#define PROBLEM36_HPP


#include <string>
#include <vector>
#include "Problem.hpp"


class Problem36 : public Problem{
private:
	//Variables
	//Static variables
	static int MAX_NUM;	//The largest number that will be checked
	//Instance variables
	std::vector<int> palindromes;	//All numbers that are palindromes in base 10 and 2
	int sum;	//The sum of all elements in the vector palindromes
public:
	//Functions
	//Constructor
	Problem36();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Returns a string with the solution to the problem
	std::vector<int> getPalindromes() const;	//Return the vector of palindromes < MAX_NUM
	int getSumOfPalindromes() const;	//Return the sum of all elements in the vector of palindromes
};


/* Results:
The sum of all base 10 and base 2 palindromic numbers < 999999 is 872187
It took an average of 22.578 milliseconds to run this problem over 100 iterations
*/


#endif //PROBLEM36_HPP
