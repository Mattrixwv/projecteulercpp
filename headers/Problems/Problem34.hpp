//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem34.hpp
//Matthew Ellison
// Created: 06-01-21
//Modified: 07-02-21
//Find the sum of all numbers which are equal to the sum of the factorial of their digits
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM34_HPP
#define PROBLEM34_HPP


#include <string>
#include <vector>
#include "Problem.hpp"


class Problem34 : public Problem{
private:
	//Variables
	//Static variables
	static int MAX_NUM;	//The largest num that can be the sum of its own digits
	//Instance variables
	std::vector<int> factorials;	//Holds the pre-computed factorials of the numbers 0-9
	int sum;	//Holds the sum of all numbers equal to the sum of their digit's factorials
public:
	//Constructor
	Problem34();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	std::vector<int> getFactorials() const;	//Returns the list of factorials from 0-9
	int getSum() const;	//Returns the sum of all numbers equal to the sum of their digit's factorials
};


/* Results:
The sum of all numbers that are the sum of their digit's factorials is 40730
It took an average of 15.181 milliseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM34_HPP
