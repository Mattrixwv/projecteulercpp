//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem23.hpp
//Matthew Ellison
// Created: 11-09-18
//Modified: 07-02-21
//Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM23_HPP
#define PROBLEM23_HPP


#include <cinttypes>
#include <string>
#include <vector>
#include "Problem.hpp"


class Problem23 : public Problem{
private:
	//Variable
	//Static variables
	static int MAX_NUM;	//The largest possible number that can not be written as the sum of two abundant numbers
	//Instance variables
	std::vector<uint64_t> divisorSums;	//This gives the sum of the divisors at subscripts
	uint64_t sum;	//The sum of all the numbers we are looking for

	//Functions
	bool isSum(const std::vector<int>& abund, int num);	//A function that returns true if num can be created by adding two elements from abund and false if it cannot
	void reserveVectors();	//Reserve the size of the vector to speed up insertion
public:
	//Constructor
	Problem23();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	uint64_t getSum() const;	//Returns the sum of the numbers asked for
};


/* Results:
The answer is 4179871
It took an average of 5.902 seconds to run this problem over 100 iterations
*/


#endif	//PROBLEM23_HPP
