//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem28.hpp
//Matthew Ellison
// Created: 09-21-19
//Modified: 07-02-21
//What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed by starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM28_HPP
#define PROBLEM28_HPP


#include <cinttypes>
#include <string>
#include <vector>
#include "Problem.hpp"


class Problem28 : public Problem{
private:
	//Variables
	//Instance variables
	std::vector<std::vector<int>> grid;	//Holds the grid that we will be filling and searching
	uint64_t sumOfDiagonals;	//Holds the sum of the diagonals of the grid

	//Functions
	void setupGrid();	//This sets up the grid to hold the correct number of variables
	void createGrid();	//Puts all of the numbers in the grid up the grid
	void findSum();	//Finds the sum of the diagonals in the grid
public:
	//Constructor
	Problem28();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	std::vector<std::vector<int>> getGrid() const;	//Returns the grid
	uint64_t getSum() const;	//Returns the sum of the diagonals
};


/* Results:
The sum of the diagonals in the given grid is 669171001
It took an average of 1.254 milliseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM28_HPP
