//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem2.hpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//The sum of the even Fibonacci numbers less than 4,000,000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM2_HPP
#define PROBLEM2_HPP


#include <cinttypes>
#include <string>
#include "Problem.hpp"


class Problem2 : public Problem{
private:
	//Variables
	//Static variables
	static uint64_t TOP_NUM;	//Holds the largest number that we are looking for
	//Instance variables
	uint64_t fullSum;	//Holds the sum of all the numbers
public:
	//Constructor
	Problem2();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	uint64_t getSum() const;	//Returns the requested sum
};


/* Results:
The sum of the even Fibonacci numbers less than 4,000,000 is 4613732
It took an average of 324.000 nanoseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM2_HPP
