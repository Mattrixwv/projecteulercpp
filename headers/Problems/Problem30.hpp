//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem30.hpp
//Matthew Ellison
// Created: 10-27-19
//Modified: 07-02-21
//Find the sum of all the numbers that can be written as the sum of the fifth powers of their digits.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM30_HPP
#define PROBLEM30_HPP


#include <cinttypes>
#include <string>
#include <vector>
#include "Problem.hpp"


class Problem30 : public Problem{
private:
	//Variables
	//Static variables
	static uint64_t TOP_NUM;	//This is the largest number that will be checked
	static uint64_t BOTTOM_NUM;	//Start with 2 because 0 and 1 don't count
	static uint64_t POWER_RAISED;	//This is the power that the digits are raised to
	//Instance variables
	std::vector<uint64_t> sumOfFifthNumbers;	//This is a vector of the numbers that are the sum of the fifth power of their digits
	uint64_t sum;	//This is the sum of the sumOfFifthNumbers vector

	//Functions
	std::vector<uint64_t> getDigits(uint64_t num);	//Returns a vector with the indivitual digits of the number passed into it
public:
	//Constructor
	Problem30();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	uint64_t getTopNum() const;	//This returns the top number to be checked
	std::vector<uint64_t> getListOfSumOfFifths() const;	//This returns a copy of the vector holding all the numbers that are the sum of the fifth power of their digits
	uint64_t getSumOfList() const;	//This returns the sum of all entries in sumOfFifthNumbers
};


/* Results:
The sum of all the numbers that can be written as the sum of the fifth powers of their digits is 443839
It took an average of 306.944 milliseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM30_HPP
