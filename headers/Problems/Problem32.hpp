//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem32.hpp
//Matthew Ellison
// Created: 07-27-20
//Modified: 07-02-21
//Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM32_HPP
#define PROBLEM32_HPP


#include <cinttypes>
#include <string>
#include <vector>
#include "Problem.hpp"


class Problem32 : public Problem{
private:
	//Structures
	struct ProductSet{
		int multiplicand;
		int multiplier;
		ProductSet(int multiplicand, int multiplier) : multiplicand(multiplicand), multiplier(multiplier){
		}
		int64_t getProduct(){
			return ((int64_t)multiplicand * (int64_t)multiplier);
		}
		bool operator==(ProductSet secondSet){
			return (getProduct() == secondSet.getProduct());
		}
		std::string getNumString(){
			return std::to_string(multiplicand) + std::to_string(multiplier) + std::to_string(getProduct());
		}
	};

	//Variables
	//Static variables
	static int TOP_MULTIPLICAND;	//The largest multiplicand to check
	static int TOP_MULTIPLIER;		//The largest multiplier to check
	//Instance variables
	std::vector<ProductSet> listOfProducts;	//The list of unique products that are 1-9 pandigital
	uint64_t sumOfPandigitals;	//The sum of the products of the pandigital numbers

	//Functions
	//Returns true if the passed productset is 1-9 pandigital
	bool isPandigital(ProductSet currentSet);
public:
	//Functions
	//Constructor
	Problem32();
	//Operational functions
	void solve();	//Solve the problem
	void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	int64_t getSumOfPandigitals() const;	//Returns the sum of the pandigitals
};

/* Results:
There are 7 unique 1-9 pandigitals
The sum of the products of these pandigitals is 45228
It took an average of 4.480 milliseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM32_HPP
