//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem16.hpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//What is the sum of the digits of the number 2^1000?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
//This file contains a header from the gmp library. The library is used for large integers.
//You can find more information about them at https://gmplib.org/
//When compiling this file you need to have the gmp library installed as well as linking the libraries to your executable using the -lgmpxx and -lgmp flags
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM16_HPP
#define PROBLEM16_HPP


#include <string>
#include <gmpxx.h>
#include "Problem.hpp"


class Problem16 : public Problem{
private:
	//Variables
	//Static variables
	static int NUM_TO_POWER;	//The number that is going to be raised to a power
	static int POWER;	//The power that the number is going to be raised to
	//Instance variables
	mpz_class num;	//The number to be calculated
	int sumOfElements;	//The sum of all digits in the number
public:
	//Constructors
	Problem16();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	mpz_class getNumber() const;	//Returns the number that was calculated
	int getSum() const;		//Return the sum of the digits of the number
};


/* Results:
2^1000 = 10715086071862673209484250490600018105614048117055336074437503883703510511249361224931983788156958581275946729175531468251871452856923140435984577574698574803934567774824230985421074605062371141877954182153046474983581941267398767559165543946077062914571196477686542167660429831652624386837205668069376
The sum of the elements is 1366
It took an average of 4.806 microseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM16_HPP
