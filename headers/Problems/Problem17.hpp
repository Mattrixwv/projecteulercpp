//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem17.hpp
//Matthew Ellison
// Created: 10-05-18
//Modified: 07-02-21
//If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PROBLEM17_HPP
#define PROBLEM17_HPP


#include <cinttypes>
#include <string>
#include "Problem.hpp"


class Problem17 : public Problem{
private:
	//Variables
	//Static variables
	static int START_NUM;	//This is the smallest number to get the words of
	static int STOP_NUM;	//This is the largest number to get the words of
	//Instance variables
	uint64_t letterCount;	//This is the cumulative number of letters in the words of the numbers

	//Functions
	std::string makeWordFromNum(int number);	//This function makes a word out of the number passed into it
	std::string wordHelper(int num);	//This function helps makeWordFromNum() by returning the words for the numbers 1-9
	uint64_t countLetters(std::string str);	//This counts the number of letters in the string that is passed in (ignoring numbers and punctuation)
public:
	//Constructor
	Problem17();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	uint64_t getLetterCount() const;	//Returns the number of letters asked for
};


/* Results:
The number of letters is 21124
It took an average of 220.126 microseconds to run this problem over 100 iterations
*/


#endif	//Problem17_HPP
