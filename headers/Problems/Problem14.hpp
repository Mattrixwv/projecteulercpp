//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem14.hpp
//Matthew Ellison
// Created: 09-29-18
//Modified: 07-02-21
/*
The following iterative sequence is defined for the set of positive integers:
n → n/2 (n is even)
n → 3n + 1 (n is odd)
Which starting number, under one million, produces the longest chain?
*/
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM14_HPP
#define PROBLEM14_HPP


#include <cinttypes>
#include <string>
#include "Problem.hpp"


class Problem14 : public Problem{
private:
	//Variables
	//Static variables
	static uint64_t MAX_NUM;	//This is the top number that you will be checking against the series
	//Instance variables
	uint64_t maxLength;	//This is the length of the longest chain
	uint64_t maxNum;	//This is the starting number of the longest chain

	//Function
	uint64_t checkSeries(uint64_t num);	//This function follows the rules of the sequence and returns its length
public:
	//Constructor
	Problem14();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	uint64_t getLength() const;		//Returns the length of the requested chain
	uint64_t getStartingNumber() const;	//Returns the starting number of the requested chain
};


/* Results:
The number 837799 produced a chain of 525 steps
It took an average of 197.008 milliseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM14_HPP
