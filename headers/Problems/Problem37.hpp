//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem37.hpp
//Matthew Ellison
// Created: 06-30-21
//Modified: 07-02-21
//Find the sum of the only eleven primes that are both truncatable from left to right and right to left (2, 3, 5, and 7 are not counted).
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM37_HPP
#define PROBLEM37_HPP


#include <cinttypes>
#include <string>
#include <vector>
#include "Problem.hpp"


class Problem37 : public Problem{
private:
	//Variables
	//Static variables
	static uint64_t LAST_PRIME_BEFORE_CHECK;	//The last prime before 11 since single digit primes aren't checked
	//Instance variables
	std::vector<uint64_t> truncPrimes;	//All numbers that are truncatable primes
	uint64_t sum;	//The sum of all elements in truncPrimes
public:
	//Functions
	//Constructor
	Problem37();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Returns a string with the solution to the problem
	std::vector<uint64_t> getTruncatablePrimes() const;	//Returns the list of primes that can be truncated
	uint64_t getSumOfPrimes() const;	//Get the sum of all primes in truncPrimes
};


/* Results:
The sum of all left and right truncatable primes is 748317
It took an average of 63.831 milliseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM37_HPP
