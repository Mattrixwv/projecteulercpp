//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem6.hpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM6_HPP
#define PROBLEM6_HPP


#include <cinttypes>
#include <string>
#include "Problem.hpp"


class Problem6 : public Problem{
private:
	//Variables
	//Static variables
	static int START_NUM;	//The first number to check
	static int END_NUM;		//The last number to check
	//Instance variables
	uint64_t sumOfSquares;	//Holds the sum of the squares of all the numbers
	uint64_t squareOfSum;	//Holds the square of the sum of all the numbers
public:
	//Constructor
	Problem6();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	uint64_t getSumOfSquares() const;	//Returns the sum of all the squares
	uint64_t getSquareOfSum() const;	//Returns the square of all of the sums
	uint64_t getDifference() const;		//Returns the requested difference
};


/* Result:
The difference between the sum of the squares and the square of the sum of all numbers from 1-100 is 25164150
It took an average of 76.000 nanoseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM6_HPP
