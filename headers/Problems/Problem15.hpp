//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem15.hpp
//Matthew Ellison
// Created: 09-29-18
//Modified: 07-02-21
//How many routes from the top left corner to the bottom right corner are there through a 20×20 grid if you can only move right and down?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PROBLEM15_HPP
#define PROBLEM15_HPP


#include <cinttypes>
#include <string>
#include "Problem.hpp"


class Problem15 : public Problem{
private:
	//Variables
	//Static variables
	static int WIDTH;	//The width of the grid
	static int LENGTH;	//The length of the grid
	//Instance variables
	uint64_t numOfRoutes;	//The number of routes from 0, 0 to 20, 20

	//Functions
	//This function acts as a handler for moving the position on the grid and counting the distance
	//It moves right first, then down
	void move(int currentX, int currentY);
public:
	//Constructor
	Problem15();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	uint64_t getNumberOfRoutes() const;		//Returns the number of routes found
};


/* Results:
The number of routes is 137846528820
It took an average of 18.010 minutes to run this problem through 10 iterations
*/


#endif	//PROBLEM15_HPP
