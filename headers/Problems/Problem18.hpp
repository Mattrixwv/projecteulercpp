//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem18.hpp
//Matthew Ellison
// Created: 11-01-18
//Modified: 07-02-21
//Find the maximum total from top to bottom
/*
75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
*/
//This is done using a breadth first search
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM18_HPP
#define PROBLEM18_HPP


#include <list>
#include <string>
#include <vector>
#include "Problem.hpp"


class Problem18 : public Problem{
private:
	//Structures
	//A structure to hold the location data for a point on the map
	struct location{
		int xLocation;
		int yLocation;
		int total;
		bool fromRight;
		location(int x, int y, int t, bool r) : xLocation(x), yLocation(y), total(t), fromRight(r){}
	};

	//Variables
	//Instance variables
	std::list<location> foundPoints;	//For the points that I have already found the shortest distance to
	std::list<location> possiblePoints;	//For the locations you are checking this round
	int actualTotal;	//The true total of the path from the top to the bottom

	//Functions
	void invert();	//This list turns every number in the vector into 100 - num
	void setupList();	//Setup the list of numbers
protected:
	//Variables
	//Static variables
	static std::vector<std::vector<int>> list;	//Setup the list you are trying to find a path through
public:
	//Constructor
	Problem18();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	std::string getPyramid() const;	//Returns the pyramid that was traversed as a string
	std::string getTrail() const;	//Returns the trail the algorithm took as a string
	int getTotal() const;	//Returns the total that was asked for
};

/* Results:
The value of the longest path is 1074
It took an average of 9.925 microseconds to run this problem over 100 iterations
*/

#endif	//PROBLEM18_HPP
