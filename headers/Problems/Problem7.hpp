//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem7.hpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//What is the 10001th prime number?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM7_HPP
#define PROBLEM7_HPP


#include <cinttypes>
#include <string>
#include <vector>
#include "Problem.hpp"


class Problem7: public Problem{
private:
	//Variables
	//Static variables
	static uint64_t NUMBER_OF_PRIMES;	//The index of the prime number to find
	//Instance variables
	std::vector<uint64_t> primes;	//Holds the prime numbers
public:
	//Constructor
	Problem7();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	uint64_t getPrime() const;	//Returns the requested prime number
};


/* Results:
The 10001th prime number is 104743
It took an average of 3.864 milliseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM7_HPP
