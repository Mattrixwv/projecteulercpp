//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem10.hpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//Find the sum of all the primes below two million
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM10_HPP
#define PROBLEM10_HPP


#include <cinttypes>
#include <string>
#include "Problem.hpp"


class Problem10 : public Problem{
private:
	//Variables
	//Static variables
	static uint64_t GOAL_NUMBER;	//The largest number to check for primes
	//Instance variables
	uint64_t sum;	//The sum of all of the prime
public:
	//Constructor
	Problem10();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	uint64_t getSum() const;	//Returns the sum that was requested
};


/* Results:
The sum of all the primes less than 2000000 is 142913828922
It took an average of 171.141 milliseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM10_HPP
