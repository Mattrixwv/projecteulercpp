//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem1.hpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//What is the sum of all the multiples of 3 or 5 that are less than 1000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM1_HPP
#define PROBLEM1_HPP


#include <cinttypes>
#include <string>
#include "Problem.hpp"


class Problem1: public Problem{
private:
	//Variables
	//Static variables
	static uint64_t MAX_NUMBER;	//The highest number to be tested
	//Instance variables
	uint64_t fullSum;		//For the sum of all the numbers
	uint64_t sumOfProgression(uint64_t multiple);	//Gets the sum of the progression of the multiple
public:
	//Constructor
	Problem1();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	uint64_t getSum() const;	//Returns the requested sum
};


/* Results:
The sum of all the numbers < 1000 that are divisible by 3 or 5 is 233168
It took an average of 50.000 nanoseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM1_HPP
