//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem26.hpp
//Matthew Ellison
// Created: 07-28-19
//Modified: 07-02-21
//Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM26_HPP
#define PROBLEM26_HPP


#include <string>
#include "Problem.hpp"


class Problem26 : public Problem{
private:
	//Variables
	//Static variables
	static unsigned int TOP_NUMBER;	//Holds the highest denominator we will check
	//Instance variables
	unsigned int longestCycle;	//The length of the longest cycle
	unsigned int longestNumber;	//The starting denominator of the longest cycle
public:
	//Constructor
	Problem26();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	unsigned int getLongestCycle() const;	//Returns the length of the longest cycle
	unsigned int getLongestNumber() const;	//Returns the denominator that starts the longest cycle
};


/* Results:
The longest cycle is 982 digits long
It is started with the number 983
It took an average of 9.989 milliseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM26_HPP
