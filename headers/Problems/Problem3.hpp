//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem3.hpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//The largest prime factor of 600851475143
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM3_HPP
#define PROBLEM3_HPP


#include <cinttypes>
#include <string>
#include <vector>
#include "Problem.hpp"


class Problem3 : public Problem{
private:
	//Variables
	//Static variables
	static uint64_t GOAL_NUMBER;	//The number of which you are trying to find the factors
	//Instance variables
	std::vector<uint64_t> factors;	//Holds the factors of goalNumber
public:
	//Constructor
	Problem3();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	std::vector<uint64_t> getFactors() const;	//Returns the list of factors of the number
	uint64_t getLargestFactor() const;	//Returns the largest factor of the number
};


/* Results:
The largest factor of the number 600851475143 is 6857
It took an average of 50.300 milliseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM3_HPP
