//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem25.hpp
//Matthew Ellison
// Created: 11-13-18
//Modified: 07-02-21
//What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
//This file contains a header from the gmp library. The library is used for large integers.
//You can find more information about them at https://gmplib.org/
//When compiling this file you need to have the gmp library installed as well as linking the libraries to your executable using the -lgmpxx and -lgmp flags
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM25_HPP
#define PROBLEM25_HPP


#include <string>
#include <gmpxx.h>
#include "Problem.hpp"


class Problem25 : public Problem{
private:
	//Variables
	//Static variables
	static unsigned int NUM_DIGITS;	//The number of digits to calculate up to
	//Instance variables
	mpz_class number;	//The current Fibonacci number
	mpz_class index;	//The index of the current Fibonacci number just calculated
public:
	//Constructor
	Problem25();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	mpz_class getNumber() const;	//Returns the Fibonacci number asked for
	std::string getNumberString() const;	//Returns the Fibonacci number asked for as a string
	mpz_class getIndex() const;	//Returns the index of the requested Fibonacci number
	std::string getIndexString() const;	//Returns the index of the requested Fibonacci number as a string
	uint64_t getIndexInt() const;	//Returns the index of the requested Fibonacci number as a uint64_t
};

/* Results:
The first Fibonacci number with 1000 digits is 1070066266382758936764980584457396885083683896632151665013235203375314520604694040621889147582489792657804694888177591957484336466672569959512996030461262748092482186144069433051234774442750273781753087579391666192149259186759553966422837148943113074699503439547001985432609723067290192870526447243726117715821825548491120525013201478612965931381792235559657452039506137551467837543229119602129934048260706175397706847068202895486902666185435124521900369480641357447470911707619766945691070098024393439617474103736912503231365532164773697023167755051595173518460579954919410967778373229665796581646513903488154256310184224190259846088000110186255550245493937113651657039447629584714548523425950428582425306083544435428212611008992863795048006894330309773217834864543113205765659868456288616808718693835297350643986297640660000723562917905207051164077614812491885830945940566688339109350944456576357666151619317753792891661581327159616877487983821820492520348473874384736771934512787029218636250627816
Its index is 4782
It took an average of 241.017 milliseconds to run this problem over 100 iterations
*/

#endif	//PROBLEM25_HPP
