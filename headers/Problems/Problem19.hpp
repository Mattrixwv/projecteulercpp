//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem19.hpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
/*
You are given the following information, but you may prefer to do some research for yourself.
1 Jan 1900 was a Monday.
Thirty days has September,
April, June and November.
All the rest have thirty-one,
Saving February alone,
Which has twenty-eight, rain or shine.
And on leap years, twenty-nine.
A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
*/
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM19_HPP
#define PROBLEM19_HPP


#include <cinttypes>
#include <string>
#include "Problem.hpp"


class Problem19 : public Problem{
private:
	//Variables
	//Staic variables
	//An easier way to return the days
	enum DAYS {SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, NUMBER_OF_DAYS, ERROR};
	static unsigned int START_YEAR;	//The start year
	static unsigned int END_YEAR;	//The stop year
	//Instance variables
	uint64_t totalSundays;	//Keep track of the number of sundays

	//Functions
	DAYS getDay(unsigned int month, unsigned int day, unsigned int year);	//Return the day of the week that the date you pass into it is on
	bool isLeapYear(unsigned int year);	//Returns true if the year passed to it is a leap year
public:
	//Constructors
	Problem19();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	uint64_t getTotalSundays() const;	//Returns the total sundays that were asked for
};


/* Results
There are 171 Sundays that landed on the first of the months from 1901 to 2000
It took an average of 1.400 milliseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM19_HPP
