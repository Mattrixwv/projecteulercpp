//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem22.hpp
//Matthew Ellison
// Created: 11-09-18
//Modified: 07-02-21
//What is the total of all the name scores in the file?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM22_HPP
#define PROBLEM22_HPP


#include <string>
#include <vector>
#include <cinttypes>
#include "Problem.hpp"


class Problem22 : public Problem{
private:
	//Variables
	//Instance variables
	std::vector<std::string> names;	//Holds the names that will be scored
	std::vector<uint64_t> sums;	//Holds the score based on the sum of the characters in the name
	std::vector<uint64_t> prod;	//Holds the score based on the sum of the characters and the location in alphabetical order
	uint64_t sum;	//Holds the sum of the scores

	//Functions
	void getNames();	//Take the names from the file and insert them into the vector
	void reserveVectors();	//Reserve the size of the vector to speed up insertion
public:
	//Constructor
	Problem22();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	std::vector<std::string> getNames() const;	//Returns the vector of the names being scored
	uint64_t getNameScoreSum() const;	//Returns the sum of the names scores
};


/* Results:
The answer to the question is 871198282
It took an average of 569.874 microseconds to run this problem over 100 iterations
*/


#endif	//Problem22
