//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem20.hpp
//Matthew Ellison
// Created: 11-07-18
//Modified: 07-02-21
//What is the sum of the digits of 100!?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
//This file contains a header from the gmp library. The library is used for large integers.
//You can find more information about them at https://gmplib.org/
//When compiling this file you need to have the gmp library installed as well as linking the libraries to your executable using the -lgmpxx and -lgmp flags
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM20_HPP
#define PROBLEM20_HPP


#include <cinttypes>
#include <string>
#include "gmpxx.h"
#include "Problem.hpp"


class Problem20 : public Problem{
private:
	//Variables
	//Instance variables
	mpz_class num;	//Holds the number 100!
	uint64_t sum;	//The sum of the digits of num
public:
	//Constructor
	Problem20();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	mpz_class getNumber() const;	//Returns the number 100!
	std::string getNumberString() const;	//Returns the number 100! in a string
	uint64_t getSum() const;	//Returns the sum of the digits of 100!
};


/* Results:
100! = 93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000
The sum of the digits is: 648
It took an average of 4.094 microseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM20_HPP
