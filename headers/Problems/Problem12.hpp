//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem12.hpp
//Matthew Ellison
// Created: 09-27-18
//Modified: 07-02-21
//What is the value of the first triangle number to have over five hundred divisors?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM12_HPP
#define PROBLEM12_HPP


#include <cinttypes>
#include <string>
#include <vector>
#include "Problem.hpp"


class Problem12 : public Problem{
private:
	//Variables
	//Static variables
	static uint64_t GOAL_DIVISORS;	//The number of divisors that you want
	//Instance variables
	int64_t sum;		//The sum of the numbers up to counter
	int64_t counter;	//The next number to be added to sum
	std::vector<int64_t> divisors;	//Holds the divisors of the triangular number sum
public:
	//Constructor
	Problem12();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	int64_t getTriangularNumber() const;	//Returns the triangular number
	int64_t getLastNumberAdded() const;		//Get the final number that was added to the triangular number
	std::vector<int64_t> getDivisorsOfTriangularNumber() const;	//Returns the list of divisors of the requested number
	size_t getNumberOfDivisors() const;		//Returns the number of divisors of the requested number
};


/* Results:
The triangular number 76576500 is a sum of all numbers >= 12375 and has 576 divisors
It took an average of 280.536 milliseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM12_HPP
