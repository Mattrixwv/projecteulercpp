//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem27.hpp
//Matthew Ellison
// Created: 09-14-19
//Modified: 07-03-21
//Find the product of the coefficients, |a| < 1000 and |b| <= 1000, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n=0.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM27_HPP
#define PROBLEM27_HPP


#include <cinttypes>
#include <string>
#include <vector>
#include "Problem.hpp"


class Problem27 : public Problem{
private:
	//Variables
	//Instance variables
	int64_t topA;	//The A for the most n's generated
	int64_t topB;	//The B for the most n's generated
	int64_t topN;	//The most n's generated
	std::vector<int64_t> primes;	//A list of all primes that could possibly be generated with this formula
public:
	//Constructor
	Problem27();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	int64_t getTopA() const;	//Returns the top A that was generated
	int64_t getTopB() const;	//Returns the top B that was generated
	int64_t getTopN() const;	//Returns the top N that was generated
	int64_t getProduct() const;	//Returns the product of A and B for the answer
};


/* Results:
The greatest number of primes found is 70
It was found with A = -61, B = 971
The product of A and B is -59231
It took an average of 14.261 milliseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM27_HPP
