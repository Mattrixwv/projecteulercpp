//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem24.hpp
//Matthew Ellison
// Created: 11-11-18
//Modified: 07-02-21
//What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM24_HPP
#define PROBLEM24_HPP


#include <string>
#include <vector>
#include "Problem.hpp"


class Problem24 : public Problem{
private:
	//Variables
	//Static variables
	static int NEEDED_PERM;	//The number of the permutation that you need
	static std::string nums;	//All of the characters that we need to get the permutations of
	//Instance variables
	std::vector<std::string> permutations;	//Holds all of the permutations of the string nums
public:
	//Constructor
	Problem24();
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	std::vector<std::string> getPermutationsList() const;	//Returns a vector with all of the permutations
	std::string getPermutation() const;		//Returns the specific permutations you are looking for
};


/* Results
The 1 millionth permutation is 2783915460
It took an average of 1.157 seconds to run this problem over 100 iterations
*/


#endif	//PROBLEM24_HPP
