//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem5.hpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM5_HPP
#define PROBLEM5_HPP


#include <string>
#include "Problem.hpp"


class Problem5 : public Problem{
private:
	//Variables
	//Instance variables
	int smallestNum;	//The smallest number that is found
public:
	//Constructor
	Problem5();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	int getNumber() const;	//Returns the requested number
};


/* Results:
The smallest positive number evenly divisible by all numbers 1-20 is 232792560
It took an average of 1.802 seconds to run this problem over 100 iterations
*/


#endif	//PROBLEM5_HPP
