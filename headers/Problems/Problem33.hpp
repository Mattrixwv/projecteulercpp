//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem33.hpp
//Matthew Ellison
// Created: 02-05-21
//Modified: 07-03-21
/*
The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s
We shall consider fractions like, 30/50 = 3/5, to be trivial examples
There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator
If the product of these four fractions is given in its lowest common terms, find the value of the denominator
*/
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM33_HPP
#define PROBLEM33_HPP


#include <string>
#include <vector>
#include "Problem.hpp"


class Problem33: public Problem{
private:
	//Variables
	//Static variables
	static int MIN_NUMERATOR;	//The lowest the numerator can be
	static int MAX_NUMERATOR;	//The highest the numerator can be
	static int MIN_DENOMINATOR;		//The lowest the denominator can be
	static int MAX_DENOMINATOR;		//The highest the denominator can be
	//Instance variables
	std::vector<int> numerators;	//Holds the numerators that were found
	std::vector<int> denominators;	//Holds the denominators that were found
	int prodDenominator;	//Holds the answer to the question
public:
	//Constructor
	Problem33();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	std::vector<int> getNumerators() const;	//Returns the list of numerators
	std::vector<int> getDenominators() const;	//Returns the list of denominators
	int getProdDenominator() const;	//Returns the answer to the question
};


/* Results:
The denominator of the product is 100
It took an average of 69.741 microseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM33_HPP
