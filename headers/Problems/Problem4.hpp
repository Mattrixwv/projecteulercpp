//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem4.hpp
//Matthew Ellison
// Created: 09-28-18
//Modified: 07-02-21
//Find the largest palindrome made from the product of two 3-digit numbers
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM4_HPP
#define PROBLEM4_HPP


#include <cinttypes>
#include <string>
#include <vector>
#include "Problem.hpp"


class Problem4 : public Problem{
private:
	//Variables
	//Static variables
	static int START_NUM;	//The first number to check
	static int END_NUM;		//The last number to check
	//Instance variables
	std::vector<uint64_t> palindromes;	//Holds all numbers that turn out to be palindromes
public:
	//Constructor
	Problem4();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	std::vector<uint64_t> getPalindromes() const;	//Returns the list of all palindromes
	uint64_t getLargestPalindrome() const;	//Returns the largest palindrome
};


/* Results:
The largest palindrome is 906609
It took an average of 36.525 milliseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM4_HPP
