//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem35.hpp
//Matthew Ellison
// Created: 06-02-21
//Modified: 07-02-21
//How many circular primes are there below one million?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM35_HPP
#define PROBLEM35_HPP


#include <string>
#include <vector>
#include "Problem.hpp"


class Problem35 : public Problem{
private:
	//Variables
	//Static variables
	static int MAX_NUM;	//The largest number that we are checking for primes
	//Instance variables
	std::vector<int> primes;	//The primes below MAX_NUM
	std::vector<int> circularPrimes;	//The circular primes below MAX_NUM
	//Functions
	std::vector<std::string> getRotations(std::string str);	//Returns a list of all rotations of a string passed to it
public:
	//Constructor
	Problem35();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Returns a string with the solution to the problem
	std::vector<int> getPrimes() const;	//Returns the vector of primes < MAX_NUM
	std::vector<int> getCircularPrimes() const;	//Returns the vector of circular primes < MAX_NUM
	int getNumCircularPrimes() const;	//Returns the number of circular primes
};


/* Results:
The number of all circular prime numbers under 999999 is 55
It took an average of 2.618 seconds to run this problem over 100 iterations
*/


#endif	//PROBLEM35_HPP
