//ProjectEuler/ProjectEulerCPP/headers/Problems/Problem31.hpp
//Matthew Ellison
// Created: 06-19-20
//Modified: 07-02-21
//How many different ways can £2 be made using any number of coins?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/myClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEM31_HPP
#define PROBLEM31_HPP


#include <string>
#include "Problem.hpp"


class Problem31 : public Problem{
private:
	//Variables
	//Static variables
	static int desiredValue;	//The value of coins we want
	//Instance variables
	int permutations;	//The number of permutations that are found
public:
	//Constructor
	Problem31();
	//Operational functions
	virtual void solve();	//Solve the problem
	virtual void reset();	//Reset the problem so it can be run again
	//Gets
	virtual std::string getResult() const;	//Return a string with the solution to the problem
	int getPermutations() const;	//Returns the number of correct permutations of the coins
};


/* Results:
There are 73682 ways to make 2 pounds with the given denominations of coins
It took an average of 1.916 microseconds to run this problem over 100 iterations
*/


#endif	//PROBLEM31_HPP
