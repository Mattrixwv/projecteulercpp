//ProjectEuler/ProjectEulerCPP/headers/ProblemSelection.hpp
//Matthew Ellison
// Created: 07-08-20
//Modified: 07-02-21
//This is a header file with a few functions to help select and run problems
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PROBLEMSELECTION_HPP
#define PROBLEMSELECTION_HPP


#include <iostream>
#include "mee/vectorAlgorithms.hpp"
#include "Problems/Problem1.hpp"
#include "Problems/Problem2.hpp"
#include "Problems/Problem3.hpp"
#include "Problems/Problem4.hpp"
#include "Problems/Problem5.hpp"
#include "Problems/Problem6.hpp"
#include "Problems/Problem7.hpp"
#include "Problems/Problem8.hpp"
#include "Problems/Problem9.hpp"
#include "Problems/Problem10.hpp"
#include "Problems/Problem11.hpp"
#include "Problems/Problem12.hpp"
#include "Problems/Problem13.hpp"
#include "Problems/Problem14.hpp"
#include "Problems/Problem15.hpp"
#include "Problems/Problem16.hpp"
#include "Problems/Problem17.hpp"
#include "Problems/Problem18.hpp"
#include "Problems/Problem19.hpp"
#include "Problems/Problem20.hpp"
#include "Problems/Problem21.hpp"
#include "Problems/Problem22.hpp"
#include "Problems/Problem23.hpp"
#include "Problems/Problem24.hpp"
#include "Problems/Problem25.hpp"
#include "Problems/Problem26.hpp"
#include "Problems/Problem27.hpp"
#include "Problems/Problem28.hpp"
#include "Problems/Problem29.hpp"
#include "Problems/Problem30.hpp"
#include "Problems/Problem31.hpp"
#include "Problems/Problem32.hpp"
#include "Problems/Problem33.hpp"
#include "Problems/Problem34.hpp"
#include "Problems/Problem35.hpp"
#include "Problems/Problem36.hpp"
#include "Problems/Problem37.hpp"
#include "Problems/Problem67.hpp"


//Setup the problem numbers
std::vector<unsigned int> PROBLEM_NUMBERS = {0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10,
												11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
												21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
												31, 32, 33, 34, 35, 36, 37, 67};

//This function returns a pointer to a problem of type number
Problem* getProblem(unsigned int problemNumber){
	Problem* problem = nullptr;	//Holds the problem we are about to create

	//Decide which problem was asked for and create it
	switch(problemNumber){
		case 1 : problem = new Problem1; break;
		case 2 : problem = new Problem2; break;
		case 3 : problem = new Problem3; break;
		case 4 : problem = new Problem4; break;
		case 5 : problem = new Problem5; break;
		case 6 : problem = new Problem6; break;
		case 7 : problem = new Problem7; break;
		case 8 : problem = new Problem8; break;
		case 9 : problem = new Problem9; break;
		case 10 : problem = new Problem10; break;
		case 11 : problem = new Problem11; break;
		case 12 : problem = new Problem12; break;
		case 13 : problem = new Problem13; break;
		case 14 : problem = new Problem14; break;
		case 15 : problem = new Problem15; break;
		case 16 : problem = new Problem16; break;
		case 17 : problem = new Problem17; break;
		case 18 : problem = new Problem18; break;
		case 19 : problem = new Problem19; break;
		case 20 : problem = new Problem20; break;
		case 21 : problem = new Problem21; break;
		case 22 : problem = new Problem22; break;
		case 23 : problem = new Problem23; break;
		case 24 : problem = new Problem24; break;
		case 25 : problem = new Problem25; break;
		case 26 : problem = new Problem26; break;
		case 27 : problem = new Problem27; break;
		case 28 : problem = new Problem28; break;
		case 29 : problem = new Problem29; break;
		case 30 : problem = new Problem30; break;
		case 31 : problem = new Problem31; break;
		case 32 : problem = new Problem32; break;
		case 33 : problem = new Problem33; break;
		case 34 : problem = new Problem34; break;
		case 35 : problem = new Problem35; break;
		case 36 : problem = new Problem36; break;
		case 37 : problem = new Problem37; break;
		case 67 : problem = new Problem67; break;
	}

	//Return the newly created problem
	return problem;
}

void printDescription(Problem* problem){
	std::cout << problem->getDescription() << '\n';
}

void solveProblem(Problem* problem){
	//Print the problem description
	printDescription(problem);
	//Solve the problem
	problem->solve();
	//Print the results
	std::cout << problem->getResult()
			<< "\nIt took " << problem->getTime() << " to solve this problem.\n\n" << std::endl;
}

unsigned int getProblemNumber(){
	unsigned int problemNumber = 0;
	std::cout << "Enter a problem number: ";
	std::cin >> problemNumber;
	while(!mee::isFound(PROBLEM_NUMBERS, problemNumber) || std::cin.fail()){
		std::cout << "That is an invalid problem number!\nEnter a problem number: ";
		std::cin.clear();
		std::cin >> problemNumber;
	}
	return problemNumber;
}

void listProblems(){
	std::cout << PROBLEM_NUMBERS[1];
	for(unsigned int problemNumber = 2;problemNumber < PROBLEM_NUMBERS.size();++problemNumber){
		std::cout << ", " << PROBLEM_NUMBERS[problemNumber];
	}
	std::cout << std::endl;
}


#endif //PROBLEMSELECTION_HPP
