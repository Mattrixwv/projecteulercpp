NUMCORES = $(shell grep -c "^processor" /proc/cpuinfo)
NUMCORESWIN = ${NUMBER_OF_PROCESSORS}
LIBFLAGS = -shared -std=c++20 -O3 -fPIC -Wall -fcoroutines
EXEFLAGS = -Wall -std=c++20 -O3 -Wl,-rpath,'$$ORIGIN/lib'
LINKEDLIBS = -lgmp -lgmpxx
PROBLEM_NUMBERS = 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 67
SOURCE_DIR = src
PROBLEM_DIR = $(SOURCE_DIR)/Problems
INCLUDE_DIR = headers
PROBLEM_FILES = $(patsubst %,$(PROBLEM_DIR)/libProblem%.cpp,$(PROBLEM_NUMBERS))
LIBDIR = ./lib
LIBS = $(patsubst %, -lProblem%,$(PROBLEM_NUMBERS))

#Linux makes
all: libsMulti ProjectEuler
libs: directory $(patsubst %, $(LIBDIR)/libProblem%.so,$(PROBLEM_NUMBERS))
#Windows makes
windows: allWindows
allWindows: libsWindowsMulti ProjectEuler moveBin
libsWindows: directory $(patsubst %, $(LIBDIR)/libProblem%.dll,$(PROBLEM_NUMBERS))

#Non-build jobs
directory:
	mkdir -p $(LIBDIR)

moveBin:
	mv ProjectEuler.exe lib/ProjectEuler.exe

#Building the Libraries
$(LIBDIR)/libProblem%.so: $(PROBLEM_DIR)/Problem%.cpp
	$(CXX) $(LIBFLAGS) -o $@ $< -I $(INCLUDE_DIR) $(LINKEDLIBS)
$(LIBDIR)/libProblem67.so: $(PROBLEM_DIR)/Problem67.cpp
	$(CXX) $(LIBFLAGS) -o $@ $< -I $(INCLUDE_DIR) $(LINKEDLIBS) -L $(LIBDIR) -lProblem18
libsMulti:
	$(MAKE) libs -j $(NUMCORES)

#Building the Libraries for Windows
$(LIBDIR)/libProblem%.dll: $(PROBLEM_DIR)/Problem%.cpp
	$(CXX) $(LIBFLAGS) -o $@ $< -I $(INCLUDE_DIR) $(LINKEDLIBS)
$(LIBDIR)/libProblem67.dll: $(PROBLEM_DIR)/Problem67.cpp
	$(CXX) $(LIBFLAGS) -o $@ $< -I $(INCLUDE_DIR) $(LINKEDLIBS) -L $(LIBDIR) -lProblem18
libsWindowsMulti:
	$(MAKE) libsWindows -j $(NUMCORESWIN)

#Building the executable
ProjectEuler: $(SOURCE_DIR)/main.cpp
	$(CXX) $(EXEFLAGS) -o $@.exe $< -I $(INCLUDE_DIR) -L $(LIBDIR) $(LIBS) $(LINKEDLIBS)


#Clean up/Remove all files and folders created
.PHONY: clean

clean:
	rm -f lib/* ProjectEuler.exe
	rmdir lib
